---
title: "Modelos. Datos Temporadas"
output: 
  flexdashboard::flex_dashboard:
    css: ../../tidyverse/salida-html/estilo.css
    highlight:  espresso
    theme: darkly
    orientation: columns
    vertical_layout: scroll
---

```{r setup, include=FALSE}
library(flexdashboard)
knitr::opts_chunk$set(echo = TRUE, class.source="colum_col", class.output = "salcolor")
```


```{r include=FALSE}
library(readr)
library(tidyverse)
library(skimr)
library(magrittr)
```



Datos {data-navmenu="Intro"}
===============================================================================================

## Col 


### Exploración Datos {.chcolor}


El set de datos que importamos contiene infromación sobre los resultados de los encuentros disputados en la segunda vuelta desde la temporada 2002-03 a la temporada 2019-20 en Laliga. Además del resultado, se incluye las siguientes variables adicionales:

Denotamos:

  `Equipo A`. Al equipo que juega como Local el encuentro analizado.
  
  `Equipo B`. Al equipo que juega como Visitante el encuentro analizado.

 - **Variable respuesta**. 
    
      + **ResF**. Variable cualitativa, con 3 niveles: GANA, PIERDE, EMPATA. Donde cada observacion recoge, para el equipo A, si gana, pierde o empata el encuentro.

 - **Variables básicas**, que no requieren transformación, o la transformación es mínima.
 
    + **Jornada**. Variable numérica. Recoge el número entero de la jornada en la que se disputo el encuentro. 
   
    + **Día**: Variable numrica. La variable recoge el número entero del día (1, 2, ..., 31), en el  que se juego el encuentro.
    
    + **Mes**: Variable de numérica. La variable recoge el entero del mes (1, ..., 12), em el  que se disputo el partido.

    + **Dif_Rank**: Variable numérica. Diferencia del rango (posición en la clasificación) entre el equipo A y el equipo B, en el momento del encuentro.
    
    + **Dif_Pto**: Variable numérica. Diferencia de puntos (puntos en la clasificación) entre el equipo A y el equipo B, en el momento del encuentro.
    
  - **Variables bloque total**. Estas variables recogen diferentes proporciones y promedios entre el equipo A y el equipo B. Pero con refente a los totales, es decir, teniendo en cuenta los encuentros de local y visitante para ambos equipos.

    + **Dif_PGT**: Diferencia de la proporción del total de partidos ganados entre el equipo A y el equipo B.

    + **Dif_GFT**: Diferencia del promedio del total de goles a favor entre le equipo A y el equipo B.

    + **Dif_GCT**: Diferencia del promedio del total de goles en contra entre le equipo A y el equipo B.  

    + **Dif_racha**: Diferencia de la proporción de la racha entre el equipo A y el equipo B.  Considerándose racha a los últimos diez partidos disputados,  con el criterio  de sumar 3 puntos en caso de victoria, sumar 1 en caso de empate y restar 1 en caso de derrota.
    
 - **Varaibles bloque casa/fuera**. Estas variables recogen diferentes proporciones y promedios entre el equipo A y el equipo B. Pero con teniendo en cuenta, los partidos jugados como local del equipo A, y los partidos disputados como visitante del equipo B.

    + **Dif_PG_CF**: Diferencia de la proporción de partidos ganados, a lo largo de la temporada, del equipo A como Local y del equipo B como Visitante.

    + **Dif_GF_CF**:  Diferencia del promedio de goles a favor, a lo largo de la temporada, del equipo A como Local y del equipo B como Visitante.

    + **Dif_GC_CF**: Diferencia del promedio de goles en contra a lo largo de la temporada, del equipo A como Local y del equipo B como Visitante.

    + **Dif_racha_CF**: Diferencia de la proporción de racha de partidos ganados del equipo A como Local y la de partidos ganados del Equipo B como Visitante. Considerándose racha a los últimos diez partidos como local, en el caso del Equipo A, y los últimos cinco partidos como visitnate, en el caso del Equipo B, y con el criterio  de sumar 3 puntos en caso de victoria, sumar 1 en caso de empate y restar 1 en caso de derrota.
           



Importamos estos datos que se encuentran en  el archivo csv, `datos_difb365.csv`.

```{r}
dat_diff <- read_csv(file =  "../../data/datos_dif_mod.csv", 
                      col_types = cols(
                         Temporada = col_skip(),
                         Jornada = col_integer(),
                         Local = col_skip(),
                         Visitante = col_skip(),
                         Dia = col_integer(),
                         Mes = col_integer(),
                         Res_cuant = col_skip(),
                         ResF = col_factor(levels = NULL, ordered = FALSE, include_na = FALSE))
                    )%>%
            select(ResF, Jornada, Dia:Dif_racha_CF)
     


dat_diff %T>%                 
  glimpse()%>%
  skim()
```

&nbsp;


Valores perdidos {data-navmenu="Intro"}
===============================================================================================

## Col 


### Valores Perdidos {.chcolor}


Identificamos las variables que contienen valores perdidos. Ya que tenemos que tenerlos en cuenta en el preprocesamiento.

<div>
```{r fig.align='center'}
library(DataExplorer)
library(ggpubr)
dat_diff %T>% 
  plot_missing(
    title   = "Porcentaje de valores ausentes",
    ggtheme = theme_bw(),
    theme_config = list(legend.position = "none")
   ) %>%
  map_dbl(.f = function(x){sum(is.na(x))}) 
```
</div>

<br>

Identificamos que las variables BET365 contienen, cada una, 5 valores perdidos. Esta información debemos de tenerla en cuenta en la momento de realizar la receta de nuestro workflow. Puesto que hay modelos que no admiten valores perdidos.


Estudio de las variables {data-navmenu="Intro"}
===============================================================================================

## Col 


### Variables {.chcolor}


<h2> Variable respuesta </h2>  

La variable respuesta es `ResF`. Vamos a examinar cuantas observaciones tiene cada nivel.

<div class='col2'>
<div>
```{r}
dat_diff %$%
  fct_count(ResF)
```
</div>
<div>
```{r}
dat_diff %>%
  mutate(ResF = fct_rev(fct_infreq(ResF)))%>%
  ggplot(aes(y=ResF, fill = ResF))+
  geom_bar()+
  coord_flip()
```
</div>
</div>


<h2> Variables predictoras </h2>  

Estudio de las variables predictoras.




<div>
```{r fig.show='hold',  out.width='50%', message=FALSE, warning=FALSE}
dat_diff %>%
  pivot_longer(Dif_rank:Dif_GCT, names_to = "stat", values_to = "value") %>%
  ggplot(aes(ResF, value, fill = ResF, color = ResF)) +
  geom_boxplot(alpha = 0.4) +
  facet_wrap(~stat, scales = "free_y", nrow = 2) +
  theme (axis.text.x = element_text(size=rel(0.8), angle = 90, hjust = 0.7, vjust = 0.3))+
  labs(x = NULL, color = NULL, fill = NULL, y = NULL)+ 
  guides(color=FALSE, fill = FALSE)

dat_diff %>%
  pivot_longer(Dif_racha:Dif_racha_CF, names_to = "stat", values_to = "value") %>%
  ggplot(aes(ResF, value, fill = ResF, color = ResF)) +
  geom_boxplot(alpha = 0.4) +
  facet_wrap(~stat, scales = "free_y", nrow = 2) +
  theme (axis.text.x = element_text(size=rel(0.8), angle = 90, hjust = 0.7, vjust = 0.3))+
  labs(y = NULL, color = NULL, fill = NULL)+ 
  guides(color=FALSE, fill = FALSE)
```
</div>



Modelo C5.0 {data-navmenu="arbol básico"}
===============================================================================================

## Col 


### Construcción workflow {.chcolor}

<h2>Introducción tidymodels</h2>

```{r}
library(tidymodels)
```


Vamos a usar la libreria `tidymodels` para crear nuestro flujo de trabajo. Esta libreria carga a su vez otras librerías, como son `rsample`, `recipe`, `parsnip`, `workflows` , `tune`, `yardstick`, y otras, pero nosotros vamos a utilizar solo estas seis4. Se harán pequeñas aclaraciones del uso de las funciones de estas librerías en nuestro primer flujo de trabajo. 



<h2> Rsample </h2> 


Para comenzar tenemos que dividir nuestro conjunto de datos en entrenamiento y test. Esto lo conseguimos con las funciones de `rsample`. Utilizamos `initial_split`, sobre nuestro conjunto de datos, para realizar una partición binaria de nuestros datos en entranamiento y prueba. Para sacar los conjuntos de datos (train/test) utilizamos las funciones `training` sobre el initial_split(datos),  para obtener el conjunto de datos de entrenamiento y `testing` del mismo initial_split, para el conjunto de prueba.

<h2>Partición</h2>

```{r}
set.seed(123) # La partición que realiza initial_split es única. Por tanto, usamos esta semilla
diff_split <- initial_split(dat_diff, strata = ResF)
```

```{r echo= FALSE}
diff_split
```

<h2>Conjunto de entrenamiento</h2>

```{r}
diff_train <- training(diff_split)
```

```{r echo = FALSE}
diff_train %>%
  head()
```

Mostramos las proporciónes de los niveles de `ResF` que hay en el conjunto de entrenamiento.

```{r}
diff_train %$%
  fct_count(ResF)%>%
  mutate(n = round(n / sum(n),2))
```

Vemos que hay más del doble de GANA que de EMPATA, y también casi el doble de GANA que de PIERDE. Esto puede que tengamos que tenerlo en cuenta en el preprocesameinto.



<h2>Conjunto de prueba</h2>

```{r}
diff_test <-  testing(diff_split)
```


```{r echo = FALSE}
diff_test %>%
  head()
```

Mostramos las proporciónes de los niveles de `ResF` que hay en el conjunto de prueba.

```{r}
diff_test %$%
  fct_count(ResF)%>%
  mutate(n = round(n / sum(n),2))
```

Al haber introducido `strata = ResF` en el initial_split, obtenemos las mismas proporciones que en el conjunto de entrenamiento.


<h2> Recipe </h2> 

Con recipe efectuamos una receta de los pasos que de deben a aplicar a las variables en el preprocesamiento. 
A `recipe` le tenemos que introducir la formula que vamos a utilizar en el modelo,  los datos de entrenamiento. y como hemos dicho, los cambios, eliminaciones, modificaciones, etc que se le vaya a realizar a los datos. 

En este caso aplicamos.

- **step_medianimpute** sobre las variables BET365, para imputar los valores perdidos con la mediana de los datos.

- **step_lincomb** eliminamos las variables predictoras que tengan colinealidad.

- **step_center** y **step_scale**  para centrar y escalar las variables predictoras no enteras.


```{r}
diff_rec <- recipe(ResF ~ ., data = diff_train) %>% # formula: var respuesta ResF y como var predictoras el resto.
            step_downsample(ResF)%>%
            step_lincomb(all_predictors())%>%
            step_zv(all_predictors())%>%
            step_center(all_predictors())%>%
            step_scale(all_predictors())
```

```{r echo = FALSE}
diff_rec 
```


Mostramos los datos de entrenamiento después de aplicarle la receta.

```{r}
diff_rec %>% 
  prep(training = diff_train, 
       retain = TRUE) %>% 
  juice() %>%
  head()
```

y los datos de prueba con la receta aplicada.



```{r}
diff_rec %>% 
  prep(training = diff_train) %>% 
  bake(new_data = diff_test) %>%
  head()
```

La receta  a los datos de prueba se aplica después de haberla aplicado en los datos de entrenamiento. Es decir, si por ejemplo una variable se elimina por colinealidad en los datos de entrenamiento, también se elimna en los datos de prueba; si en los datos de prueba  hubiese un valor perdido de alguna varibale BET365, se imputa con la mediana de los datos de entrenmiento, e igual con el escalado y el centrado.


<h2>Parsnip </h2> 

Con parsnip construimos el modelo que vamos a usar. Tipo de modelo, motor (librería) y clase de modelo (reresión o clasificación).

En este caso, usamos:

- **decision_tree** : arbol de decisión.

- **set_engine()**:  Motor que se va a usar. Usamos `C5.0`.

- **set_mode()**- Clase de modelo. `clasificación`.

```{r}
diff_mod <- decision_tree() %>%
            set_engine("C5.0", model = TRUE)%>%
            set_mode("classification")
```

```{r echo = FALSE}
diff_mod
```


<h2>Workflow</h2> 

Para unificar todo lo realizado hasta ahora, creamos nuestro `workflow`. Y le añadimos la receta y el modelo.

```{r}
diff_wf <- workflow()%>%
           add_recipe(diff_rec)%>%
           add_model(diff_mod)
```

```{r echo=FALSE}
diff_wf
```



### Resultados modelo C5.0 {.chcolor}

<h2> Tune y yardstick </h2>

La  librería `tune` tiene la función `last_fit`. Le introducimos nuestro workflow y la partición de train/test que hicimos con rsample. También, le introducimos las métircas que será uno de los datos que sacará la función. Las métricas que vamos a analizar son la auc, sensibilidad, especificidad y la sensitividad, estas se calculan gracias a `metric_set` de la librería `yardstick`.


```{r echo=TRUE}
arbol_clasificacion <- diff_wf %>%
                       last_fit(diff_split, metrics =
                             metric_set(roc_auc, sens, spec, accuracy))
```

La función devuelve una lista que se compene a sus vez de otras listas con la informacción tanto del preprocesamiento como del análisis. 

```{r echo=FALSE}
arbol_clasificacion
```

Por ejemplo, podemos obtener de la lista que nos ha generado el `last_fit`, las `métricas` y las `prediciones`.

Si aplicapcamos `collect_metrics` (librería tune) a nuestra lista, nos devuelve las métricas que le hemos dicho que calcule.

```{r}
arbol_clasificacion %>%
  collect_metrics()
```

```{r echo= FALSE}
metricas_c5 <- arbol_clasificacion %>%
            collect_metrics()%>%
            select(.estimate)%>%
            unlist()

names(metricas_c5)<- arbol_clasificacion %>%
            collect_metrics()%>%
            select(.metric)%>%
            unlist()
```


Y con `collect_predictions` consguimos las prediciones.

```{r}
arbol_clasificacion %>%
  collect_predictions()
```



A partir de las prediciones, podemos sacar la matriz de confusión con la función `conf_mat` de la librería yardstick.
```{r fig.align='center'}
arbol_clasificacion %>%
  collect_predictions()%>%
  conf_mat(truth= ResF, estimate = .pred_class)
```

Y de la matriz de cofunsión podemos sacar su gráfico, con la función `autoplot` de la librería tune.


<div>
```{r fig.align='center'}
arbol_clasificacion %>%
  collect_predictions()%>%
  conf_mat(truth= ResF, estimate = .pred_class)%>%
  autoplot(type = "heatmap")+
  labs(title= "modelo C5.0")
```
</div>


```{r}
mconf_c5<- arbol_clasificacion %>%
            collect_predictions()%>%
            conf_mat(truth= ResF, estimate = .pred_class)%>%
            autoplot(type = "heatmap")+
            labs(title= "modelo C5.0")
```


Tuning C5.0 {data-navmenu="arbol básico"}
===============================================================================================

## Col

### Modelo C5.0 Tuning {.chcolor}



<h2> Modelo tuning </h2>  

Vamos a crear el mismo modelo, pero esta vez vamos a modificar los parámetros del arbol de decisión. 
El  parámetro que podemos modificar con el motor C5.0 es:

- **min_n**: el número mínimo de puntos de datos en un nodo que se requieren para que el nodo se divida aún más.



```{r}
diff_mod_tune <- decision_tree(min_n = tune())%>% 
                 set_engine("C5.0") %>%
                 set_mode("classification")
```


```{r echo=FALSE}
diff_mod_tune
```



<h2> workflow </h2> 

Al workflow que  habíamos  creado, podemos cambiarle el modelo que tiene por el modelo tuning, gracias a la función `update_model` de la librería tune.

```{r}
diff_wf <- diff_wf %>%
           update_model(diff_mod_tune)
```

```{r echo=TRUE}
diff_wf
```




<h2> Validación cruzada </h2> 

Para el proceso de tuning necesitamos realizar validación cruzada. Efectuamos  validación cruzada básica, sobre le conjunto de entrenamiento, con la función `vfold_cv` de rsample.

```{r}
set.seed(19378) # Con esta semilla, fijamos los 10 conjuntos que se generan con la cv
diff_folds <- vfold_cv(data = diff_train, v = 10, strata = ResF)
```

```{r echo =FALSE}
diff_folds
```


<h2>Resultado del proceso de  tuning</h2>

Para obtener el resultado del tuneado, usamos la función `tune_grid`, a la que le proporcionamos nuestro workflow y la lista generada de la validación cruzada. 

La función tune_grid, en este caso, genera 10 parámetros min_n se forma aleatoria. Y los aplica a cada folds de validación cruzada.  


```{r}
set.seed(12345)
diff_res <- diff_wf %>%
            tune_grid(resamples = diff_folds)
```

Obtenemos la siguiente lista.

```{r echo = FALSE}
diff_res 
```


<h2>Mejor auc </h2>

Tras obtener los resultados del proceso de tuning, podemos obtener con la función `select_best` el parámetro min_n con el cual se ha obtenido mejot auc.

```{r}
diff_best <- select_best(diff_res, "roc_auc")
```

```{r echo = FALSE}
diff_best
```



<h2>Workflow final</h2>

Si sobre nuestro workflow, aplicamos la función `finalize_workflow` e introducimos también el selec_best que hemos guardado. Conseguiremos nuestro workflow final, es decir, tendremos nuestro workflow, el  cual tiene nuestro modelo guardado, y  contiene  parámetro min_n con tune, se le cambiará el valor de tune, al de valor de min_n que nos a dado mejor auc en el tuning.

```{r}
diff_wf_final <- diff_wf %>%
                  finalize_workflow(diff_best) 

```

```{r echo=FALSE}
diff_wf_final
```


<h2> Tune y yardstick</h2>


Sobre estr workflow aplicamos last_fit y guardamos las métricas habituales.

```{r}
diff_fit_final <- diff_wf_final%>%
                  last_fit(diff_split, metrics =
                             metric_set(roc_auc, sens, spec, accuracy)
                  )
```





<h2>Métricas</h2>

Mostramos las métricas obtenidas del last_fit.

```{r}
diff_fit_final  %>%
          collect_metrics()
```

```{r echo=FALSE}
metricas_c5_t<- diff_fit_final  %>%
                 collect_metrics() %>%
                 select(.estimate) %>%
                 unlist()
names(metricas_c5_t)<- diff_fit_final  %>%
                 collect_metrics() %>%
                 select(.metric)%>%
                 unlist()
```


<h2>Prediciones</h2>
```{r}
diff_fit_final %>%
     collect_predictions()
```



<h2> Variables más importantes</h2>
Podemos ver cual son las variables con más importancia.
<div style="max-width: 95%;">
```{r fig.align='center'}
library(vip)
diff_wf_final %>%
        fit(diff_train) %>% # Estima los paratametros del modelo a partir de los datos de entrenamiento
        pull_workflow_fit() %>% 
        vip(geom = "point")
```
</div>



<h2>Matriz de confusión</h2>

A partri de las prediciones, podemos hallar la matriz de confusión.

```{r}
diff_fit_final %>%
        collect_predictions() %>%
        conf_mat(ResF, .pred_class)
```



<h2> Gráfico de calor</h2>

<div style="max-width: 95%;">
```{r fig.align='center'}
diff_fit_final %>%   
  collect_predictions() %>% 
  conf_mat(truth = ResF, estimate = .pred_class) %>% 
  autoplot(type = "heatmap")+
  labs(title = "Modelo C5.0 Tuning")
```
</div>



```{r echo=FALSE}
mconf_c5_t<- diff_fit_final %>%   
              collect_predictions() %>% 
              conf_mat(truth = ResF, estimate = .pred_class) %>% 
              autoplot(type = "heatmap")+
              labs(title = "Modelo C5.0 Tuning")
```



<h2> Curva roc </h2>

<div style="max-width: 95%;">
```{r fig.align='center', fig.width=10}
diff_fit_final %>%
  collect_predictions()%>%
  roc_curve(ResF, .pred_GANA, .pred_PIERDE, .pred_EMPATA)%>%
  autoplot()
```
</div>
```{r echo=FALSE}
curva_c5<- diff_fit_final %>%
            collect_predictions()%>%
            roc_curve(ResF, .pred_GANA, .pred_PIERDE, .pred_EMPATA)%>%
            autoplot()+
            labs(title = "Curva Roc- Modelo C5.0")
```

<div style="max-width: 95%;">
```{r fig.align='center'}
diff_fit_final %>%
  collect_predictions() %>%
  roc_curve(ResF, .pred_GANA,  .pred_PIERDE, .pred_EMPATA) %>%
  ggplot(aes(x = 1 - specificity, y = sensitivity)) +
  geom_line(size = 1.5, color = "midnightblue") +
  geom_abline(
    lty = 2, alpha = 0.5,
    color = "gray50",
    size = 1.2
  )
```
</div>



Boosting {data-navmenu="Boosting"}
===============================================================================================

## Col


### Modelo boosting {.chcolor}


<h2>Creaación del modelo</h2> 

Con `parsnip`creamos el modelo de boost_tree con el motor xGboost y de tipo clasificación. Modificamos el modelo del workflow anterior y lo guardamos con el nombre `diff_xb_wf`.
```{r}
diff_xb_mod <- boost_tree() %>%
                set_engine("xgboost") %>%
                set_mode("classification")
```

```{r echo=FALSE}
diff_xb_mod
```


```{r}
diff_xb_wf <- diff_wf %>%
              update_model(diff_xb_mod)
```

```{r echo=FALSE}
diff_xb_wf
```

<h2> Tune y yardstick</h2>
Al workflow actual, le realizamos el `last_fit`. Obteniendo los resultados de la aplicación del modelo, sin proseso de tuneado, a los datos entrenamiento/test.

```{r}
boost_class <- diff_xb_wf %>%
               last_fit(diff_split, metrics =
                             metric_set(roc_auc, sens, spec, accuracy))
```

```{r echo=FALSE}
boost_class
```


<h2> Métricas</h2>
Salida de las métricas obtenidas.
```{r}
boost_class  %>%
  collect_metrics()
```

```{r echo =FALSE}
metricas_xb <- boost_class %>%
            collect_metrics()%>%
            select(.estimate)%>%
            unlist()

names(metricas_xb)<- boost_class %>%
            collect_metrics()%>%
            select(.metric)%>%
            unlist()
```


<h2> Prediciones</h2>
Salida de las prediciones obetenidas
```{r}
boost_class  %>%
  collect_predictions()
```



<h2> Matriz de confusión</h2>
```{r fig.align='center'}
boost_class  %>%
  collect_predictions()%>%
  conf_mat(truth= ResF, estimate = .pred_class)
```

<div>
```{r fig.align='center'}
boost_class  %>%
  collect_predictions()%>%
  conf_mat(truth= ResF, estimate = .pred_class)%>%
  pluck("table")%>%
  as_tibble()%>%
  group_by(Truth)%>%
  mutate(prop= paste(round(n/sum(n),4)*100,"%")) %>%
  ggplot(aes(x=Truth, y = n, fill= Prediction))+
  geom_bar(stat = "identity", position = "dodge")+
  geom_text(aes(label=prop), position=position_dodge(width=0.9), vjust=-0.25)+
  labs(title = "Matriz de confusión en gráfico de barras")

```
</div>

```{r echo=FALSE}
mconf_xb<- boost_class  %>%
            collect_predictions()%>%
            conf_mat(truth= ResF, estimate = .pred_class)%>%
            autoplot(type = "heatmap")+
            labs(title = "Modelo boosting")
```



Tuning xgboost {data-navmenu="Boosting"}
===============================================================================================

## Col

### Tuning xgboost {.chcolor}



<h2>Modelo tuning xgboost</h2>

Para este tipo de modelo, los parámetros a los que se le efectuan proceso de tuning son:

- **mtry**: el número de predictores que se muestrearán aleatoriamente en cada división al crear los modelos de árbol.


- **min_n**: el número mínimo de puntos de datos en un nodo que se requieren para que el nodo se divida aún más.

- **tree_depth**: la profundidad máxima del árbol (es decir, número de divisiones).

- **learn_rate**: la velocidad a la que el algoritmo de refuerzo se adapta de iteración a iteración.

- **loss_reduction**: la reducción en la función de pérdida requerida para dividirse más.

- **sample_size**: la cantidad de datos expuestos a la rutina de ajuste

```{r}
diff_modxb_tune <- boost_tree(
                    trees = 1000,
                    tree_depth = tune(),
                    min_n = tune(),
                    loss_reduction = tune(),
                    sample_size = tune(),
                    mtry = tune(),
                    learn_rate = tune(),
                  ) %>%
                    set_engine("xgboost") %>%
                    set_mode("classification")
```


<h2>Workflow</h2>

Actualizamos el modelo que teniamos en nuestro workflow anterior, por el modelo tuning que acabamos de crear.

```{r}
diff_wf_xb <- diff_xb_wf %>%
             update_model(diff_modxb_tune)
```

```{r}
diff_wf_xb
```


<h2>Grid del tuning</h2>

Combinaciones de los parametros que se van a usa en el tuning

```{r}
set.seed(12345)
diff_grid <- grid_latin_hypercube(
  tree_depth(),
  min_n(),
  loss_reduction(),
  sample_size = sample_prop(),
  finalize(mtry(), diff_train),
  learn_rate(),
  size = 10
) 

```


```{r echo=FALSE}
diff_grid%>%
  head()
```

<h2>Resultado del proceso de  tuning</h2>
```{r}
set.seed(12345)
diff_res_xb <- diff_wf_xb %>%
                tune_grid(
                  resamples = diff_folds,
                  grid = diff_grid,
                  control = control_grid(save_pred = TRUE)
                )
```

```{r echo=FALSE}
diff_res_xb
```

<h2> Gráfico parámetros tuning</h2>
<div>
```{r fig.align='center'}
diff_res_xb %>%
  collect_metrics() %>%
  filter(.metric == "accuracy") %>%
  select(mean, mtry:sample_size) %>%
  pivot_longer(mtry:sample_size,
               values_to = "value",
               names_to = "parameter"
  ) %>%
  ggplot(aes(value, mean, color = parameter)) +
  geom_point(alpha = 0.8, show.legend = FALSE) +
  facet_wrap(~parameter, scales = "free_x") +
  labs(x = NULL, y = "AUC")
```
</div>


<h2>Mejor auc obtenido</h2>

```{r}
diff_best_xb <- select_best(diff_res_xb, "roc_auc")
```

```{r echo = FALSE}
diff_best_xb
```


<h2>Workflow final </h2>

```{r}
diff_wfxb_final <- diff_wf_xb %>%
                  finalize_workflow(diff_best_xb)
```

```{r echo=FALSE}
diff_wfxb_final
```

<h2>Tune y yardstick </h2>

```{r}
diff_fitxb_final <- diff_wfxb_final%>%
                   last_fit(diff_split, metrics =
                             metric_set(roc_auc, sens, spec, accuracy))
```


<h2>Métricas</h2>
```{r}
diff_fitxb_final  %>%
          collect_metrics()
```

```{r echo=FALSE}
metricas_xb_t <-  diff_fitxb_final  %>%
                   collect_metrics() %>%
                   select(.estimate) %>%
                   unlist()
names(metricas_xb_t)<- diff_fitxb_final  %>%
                         collect_metrics() %>%
                         select(.metric)%>%
                         unlist()
```


<h2>Prediciones</h2>
```{r}
diff_fitxb_final %>%
     collect_predictions()
```




<h2> Variables más importantes </h2>
<div style="max-width: 95%;">
```{r fig.align='center', warning=FALSE, message =FALSE}
library(vip)
diff_wfxb_final %>%
        fit(diff_train) %>%
        pull_workflow_fit() %>%
        vip(geom = "point")
```
</div>

<h2> Matriz de confusión </h2>
```{r}
diff_fitxb_final %>%
        collect_predictions() %>%
        conf_mat(ResF, .pred_class)
```



<h2> Gráfico de calor </h2>
<div>
```{r fig.align='center'}
diff_fitxb_final %>%
  collect_predictions() %>%
  conf_mat(truth = ResF, estimate = .pred_class) %>%
  autoplot(type = "heatmap")+
  labs(title="Modelo boosting Tuning")
```
</div>


```{r echo=FALSE}
mconf_xb_t<- diff_fitxb_final %>%
              collect_predictions() %>%
              conf_mat(truth = ResF, estimate = .pred_class) %>%
              autoplot(type = "heatmap")+
              labs(title="Modelo boosting Tuning")
```


<h2>Curva roc</h2>
<div style="max-width: 95%;">
```{r fig.align='center', fig.width=10}
diff_fitxb_final %>%
  collect_predictions()%>%
  roc_curve(ResF, .pred_GANA, .pred_PIERDE, .pred_EMPATA)%>%
  autoplot()
```
</div>

```{r echo=FALSE}
curva_xb<- diff_fitxb_final %>%
            collect_predictions()%>%
            roc_curve(ResF, .pred_GANA, .pred_PIERDE, .pred_EMPATA)%>%
            autoplot()+
            labs(title = "Curva ROC. Modelo boosting")
```

<div style="max-width: 95%;">
```{r fig.align='center'}
diff_fitxb_final %>%
  collect_predictions() %>%
  roc_curve(ResF, .pred_GANA,  .pred_PIERDE, .pred_EMPATA) %>%
  ggplot(aes(x = 1 - specificity, y = sensitivity)) +
  geom_line(size = 1.5, color = "midnightblue") +
  geom_abline(
    lty = 2, alpha = 0.5,
    color = "gray50",
    size = 1.2
  )
```
</div>







Random Forest {data-navmenu="Random Forest"}
===============================================================================================

## Col


### Modelo Random Forest  {.chcolor}

<h2>Parsnip </h2>

Con parsnip construimos el modelo que vamos a usar. Tipo de modelo, motor (librería) y clase de modelo (regresión o clasificación).

En este caso, usamos:

- **rand_forest** : modelo bosque aleatorio.

- **set_engine()**:  Motor que se va a usar. Usamos `C5.0`.

- **set_mode()**- Clase de modelo. `clasificación`.


```{r}
diff_rand_mod <- rand_forest() %>%
                 set_engine("ranger")%>%
                 set_mode("classification")
```

```{r echo = FALSE}
diff_rand_mod
```

<h2> Workflow </h2>

```{r}
diff_rand_wf <- diff_wf %>%
                update_model(diff_rand_mod)
```


```{r echo = FALSE}
diff_rand_wf
```

<h2> Tune y yardstick </h2>

Realizamos el `last_fit` de nuestro workflow.

```{r}
bosque_class <- diff_rand_wf %>%
                last_fit(diff_split, , metrics =
                             metric_set(roc_auc, sens, spec, accuracy)
                         )
```

```{r echo = FALSE}
bosque_class
```





<h2> Métricas </h2>

```{r}
bosque_class  %>%
  collect_metrics()
```

```{r echo=FALSE}
metricas_rand <- bosque_class %>%
            collect_metrics()%>%
            select(.estimate)%>%
            unlist()

names(metricas_rand)<- bosque_class %>%
            collect_metrics()%>%
            select(.metric)%>%
            unlist()
```


<h2> Prediciones </h2>
```{r}
bosque_class  %>%
  collect_predictions()
```





<h2> Matriz de confusion </h2>
```{r fig.align='center'}
bosque_class  %>%
  collect_predictions()%>%
  conf_mat(truth= ResF, estimate = .pred_class)
```


<h2> Gráficp de calor </h2>
<div>
```{r fig.align='center'}
bosque_class  %>%
  collect_predictions()%>%
  conf_mat(truth= ResF, estimate = .pred_class)%>%
  autoplot(type = "heatmap")
```
</div>

```{r echo=FALSE}
mconf_rand <- bosque_class  %>%
              collect_predictions()%>%
              conf_mat(truth= ResF, estimate = .pred_class)%>%
              autoplot(type = "heatmap")+
              labs(title = "Modelo ramdon")
```




Tuning Random Forest  {data-navmenu="Random Forest"}
===============================================================================================

## Col

### Tunning bosques {.chcolor}


<h2>Modelo tuning</h2>

En el caso del modelo `random Forest` los parámetros a los que podemos efecturales tuning son los siguientes.

- **mtry**: el número de predictores que se muestrearán aleatoriamente en cada división al crear los modelos de árbol.

- **trees**: el número de árboles contenidos en el conjunto.

- **min_n**: el número mínimo de puntos de datos en un nodo que se requieren para que el nodo se divida aún más.

```{r}
diff_modr_tune <- rand_forest(min_n = tune(), mtry = tune(),
                               trees = tune())%>%
                 set_engine("ranger", importance = "permutation")%>% # Añadimos importance para poder mostrar la importancia de las variables.
                 set_mode("classification")
```

```{r echo =FALSE}
diff_modr_tune
```



<h2>Workflow</h2>

Del workflow creado, vamos a actualizar el modelo que teniamos de bosque, por el modleo de tuning que hemos creado.

```{r}
diff_wf_r <- diff_rand_wf %>%
             update_model(diff_modr_tune)
```

```{r}
diff_wf_r
```



<h2>Resultado del proceso de  tuning</h2>

```{r}
set.seed(12345)
diff_res_r <- diff_wf_r %>%
              tune_grid(resamples = diff_folds)
```

```{r echo = FALSE}
diff_res_r
```

<h2> Gráfico parámetros tuning</h2>
Realizamos un gráfico del auc obtenido para cada parámetro en cada folds.
<div>
```{r fig.align='center'}
diff_res_r %>%
  collect_metrics() %>%
  filter(.metric == "roc_auc") %>%
  select(mean, mtry:min_n) %>%
  pivot_longer(mtry:min_n,
               values_to = "value",
               names_to = "parameter"
  ) %>%
  ggplot(aes(value, mean, color = parameter)) +
  geom_point(alpha = 0.8, show.legend = FALSE) +
  facet_wrap(~parameter, scales = "free_x") +
  labs(x = NULL, y = "AUC")
```
</div>




<h2>Mejor auc </h2>

Tras obtener los resultados del proceso de tuning, podemos obtener con la función `select_best` el parámetro min_n con el cual se ha obtenido mejot auc.


```{r}
diff_best_r <- select_best(diff_res_r, "roc_auc")
```

```{r echo = FALSE}
diff_best_r
```


<h2>Workflow final</h2>

Construimos nuestro wokflow final

```{r}
diff_wfr_final <- diff_wf_r %>%
                  finalize_workflow(diff_best_r)
```

```{r echo = FALSE}
diff_wfr_final
```


<h2>Tune y yardstick</h2>

Efectuamos el last_fit a nuestro work_flow final.

```{r}
diff_fitr_final <- diff_wfr_final%>%
                   last_fit(diff_split, metrics =
                             metric_set(roc_auc, sens, spec, accuracy))
```

```{r echo=FALSE}
diff_fitr_final
```


<h2> Métricas </h2>

Obtnemos las métricas.

```{r}
diff_fitr_final  %>%
          collect_metrics()
```


```{r echo=FALSE}
metricas_rand_t<- diff_fitr_final  %>%
                   collect_metrics() %>%
                   select(.estimate) %>%
                   unlist()
names(metricas_rand_t)<- diff_fitr_final  %>%
                         collect_metrics() %>%
                         select(.metric)%>%
                         unlist()
```



<h2>Prediciones </h2>

Y las prediciones.

```{r}
diff_fitr_final %>%
     collect_predictions()
```



<h2> Variables más importantes</h2>

<div style="max-width: 95%;">
```{r fig.align='center'}
library(vip)
diff_wfr_final %>%
        fit(diff_train) %>%
        pull_workflow_fit() %>%
        vip(geom = "point")

```
</div>


<h2> Matriz de confusión </h2>
```{r}
diff_fitr_final %>%
        collect_predictions() %>%
        conf_mat(ResF, .pred_class)
```





<h2>Gráfico de calor</h2>
<div>
```{r fig.align='center'}
diff_fitr_final %>%
  collect_predictions() %>%
  conf_mat(truth = ResF, estimate = .pred_class) %>%
  autoplot(type = "heatmap")
```
</div>

```{r echo=FALSE}
mconf_rand_t <- diff_fitr_final %>%
              collect_predictions() %>%
              conf_mat(truth = ResF, estimate = .pred_class) %>%
              autoplot(type = "heatmap")+
              labs(title = "Modelo random Tuning")
```

<h2> Curva ROC </h2>
<div style="max-width: 95%;">
```{r fig.align='center', fig.width=10}
diff_fitr_final %>%
  collect_predictions()%>%
  roc_curve(ResF, .pred_GANA, .pred_PIERDE, .pred_EMPATA)%>%
  autoplot()
```
</div>

```{r echo=FALSE}
curva_rand <- diff_fitr_final %>%
                collect_predictions()%>%
                roc_curve(ResF, .pred_GANA, .pred_PIERDE, .pred_EMPATA)%>%
                autoplot()+
                labs(title = "Curva ROC. Modelo Random Forest")
```

<div style="max-width: 95%;">
```{r fig.align='center'}
diff_fitr_final %>%
  collect_predictions() %>%
  roc_curve(ResF, .pred_GANA,  .pred_PIERDE, .pred_EMPATA) %>%
  ggplot(aes(x = 1 - specificity, y = sensitivity)) +
  geom_line(size = 1.5, color = "midnightblue") +
  geom_abline(
    lty = 2, alpha = 0.5,
    color = "gray50",
    size = 1.2
  )
```
</div>




Comparacion
===============================================================================================

## Col


### Comparación {.chcolor}

<h2> Métricas</h2>
<div style="max-width: 95%;">
```{r fig.align='center',  fig.width=10}
comp_C5_rand <- cbind(metricas_c5, metricas_c5_t, metricas_rand, metricas_rand_t,
                      metricas_xb, metricas_xb_t)%>%
                round(3)

comp_C5_rand %>% 
  as_tibble()%>%
  mutate(metricas = rownames(comp_C5_rand)) %>%
  select(metricas, metricas_c5:metricas_xb_t) %>%
   pivot_longer(metricas_c5:metricas_xb_t,
               values_to = "value",
               names_to = "modelo"
  ) %>%
  ggplot(aes(y = value, x =  str_split_fixed(modelo,"_",2)[,2], color = modelo, fill = modelo)) +
  geom_point(alpha = 0.8, show.legend = FALSE) +
  geom_text(aes(label=value), position=position_dodge(width=0.9), vjust=-0.35, color = "black", size=4)+
  facet_wrap(~metricas, scales = "free_x") +
  labs(x = NULL, y = "AUC", title = "Comparación de las métricas")+
  scale_y_continuous(limits = seq(0,10,1), )+
  labs(y = NULL, color = NULL, fill = NULL)+ 
  guides(color=FALSE, fill = FALSE)+
  theme_bw()

```
</div>

<h2> Gráfico de calor</h2>

<div class='col2'>
<div>
```{r echo = FALSE}
mconf_c5
```
</div>
<div>
```{r echo = FALSE}
mconf_c5_t
```
</div>
</div>

<div class='col2'>
<div>
```{r echo = FALSE}
mconf_xb
```
</div>
<div>
```{r echo = FALSE}
mconf_xb_t
```
</div>
</div>

<div class='col2'>
<div>
```{r echo = FALSE}
mconf_rand
```
</div>
<div>
```{r echo = FALSE}
mconf_rand_t
```
</div>
</div>

<h2> Curca Roc. Modelos tuning </h2>

<div class='col2'>
<div>
```{r echo = FALSE}
curva_c5
```
</div>
<div>
```{r echo = FALSE}
curva_xb
```
</div>
</div>

<div>

```{r fig.align='center', echo = FALSE}
curva_rand
```

</div>




