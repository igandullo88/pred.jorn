---
title: "<h1 style='color:black;'>Modelos partidos</h1>"
output: 
  html_document:
    css: ../../tidyverse/salida-html/estilo.css
    highlight:  espresso
    theme: darkly
knit: (function(inputFile, encoding) { 
         out_dir <- '../salida-modelos';
         rmarkdown::render(inputFile,
                           encoding=encoding, 
                           output_file=file.path(dirname(inputFile), out_dir, 'modelos_partidos.html')) })
---

```{r}
knitr::opts_chunk$set(echo = TRUE, class.source="colum_col", class.output = "salcolor", 
                      warning =FALSE, message = FALSE)
```

<div max-width: 95%;">

<div style= "color:black;">
Cargamos las librerías y funciones creadas que vamos a utilizar
</div">

```{r warning=FALSE, message=FALSE}
library(tidymodels)
library(tidyverse)
library(vip)
library(skimr)
library(skimr)
library(magick)
library(kableExtra)
source("../../tidyverse/funciones_auxiliares.R", encoding = "UTF-8")
```

<div style= "color:black;">
Cargamos los archivos csv, que ya hemos manipulado y exportados en otros docuemntos, para su último proceso de tratamiento.
</div>

```{r, message=FALSE}
dat_trans <- read_csv("../../data/resultados.csv", locale =  locale(encoding = "UTF-8")) 

jor_pred <- read_csv("../../data/partidos_pred.csv", locale = locale(encoding = "UTF-8")) %>%
  select(Local, Visitante) %>%
  mutate(Local = depura_equipo(Local), Visitante = depura_equipo(Visitante))
```


# Segundo proceso de tratamiento. 

<div style= "color:black;">
Las variables que conseguimos en esta segunda transformación son:

**Variable predictiva**

- **ResF**: Varaible cualitativa con tres niveles (GANA, PIERDE, EMPARA)

**Variable explicativas**

Para poder explicar mejor la definición de las variables explicativas, las hemos divido en tres bloques:

 - **Variables básicas**, que no requieren transformación, o la transformación es mínima.
   
    + **Día**: Variable de formato factor, con 7 niveles (lunes, martes, ... , domingo). La variable recoge el día que se juego el encuentro.
    
    + **Mes**: Variable de formato factor, con 12 niveles (enero, febrero, ... , diciembre). La variable recoge el mes que se disputo el partido.

    + **Dif_Rank**: Variable numérica. Guarada la diferencia del rango (posición en la clasificación) entre el equipo A y el equipo B.
    
    + **Dif_Pto**: Variable numérica. Guarada la diferencia de la puntos de la clasificaciónentre el equipo A y el equipo B.
    
  - **Variables bloque total**. Estas variables recogen diferentes proporciones y promedios entre el equipo A y el equipo B. Pero con refente a los totales, es decir, teniendo en cuenta los encuentros de local y visitante para ambos equipos.

    + **Dif_PGT**: Diferencia de la proporción del total de partidos ganados entre el equipo A y el equipo B.

    + **Dif_GFT**: Diferencia del promedio del total de goles a favor entre le equipo A y el equipo B.

    + **Dif_GCT**: Diferencia del promedio del total de goles en contra entre le equipo A y el equipo B.  

    + **Dif_racha**: Diferencia de la proporción de la racha entre el equipo A y el equipo B. Definiendo como racha los partidos ganados en los últimos cinco encuentros.
    
 - **Varaibles bloque casa/fuera**. Estas variables recogen diferentes proporciones y promedios entre el equipo A y el equipo B. Pero con teniendo en cuenta, los partidos jugados como local del equipo A, y los partidos disputados como visitante del equipo B.

    + **Dif_PG_CF**: Diferencia de la proporción de partidos ganados, a lo largo de la temporada, del equipo A como Local y del equipo B como Visitante.

    + **Dif_GF_CF**:  Diferencia del promedio de goles a favor, a lo largo de la temporada, del equipo A como Local y del equipo B como Visitante.

    + **Dif_GC_CF**: Diferencia del promedio de goles en contra a lo largo de la temporada, del equipo A como Local y del equipo B como Visitante.

    + **Dif_racha_CF**: Diferencia de la proporción de racha de partidos ganados del equipo A como Local y la de partidos ganados del Equipo B como Visitante. Considerándose racha a los últimos cinco partidos como local, en el caso del Equipo A, y los últimos cinco partidos como visitnate, en el caso del Equipo B.
           


Sacaremos un tibble por encuentro a predecir, como ya sabemos, 110 encuentros a los largo de las 11 jornadas, 10 encuentros por jornada,  que quedan por jugarse. 

Guardaremos cada tibble en una lista. Teniendo como resultado final, una lista con los 110 tibbles.
</div>


## Historico de entrentamientos por partido a predecir.



```{r warning=FALSE, message=FALSE}
encuentros <- list()

for(j in 1:nrow(jor_pred)){

# Equipos que disputan el encuentor j-ésimo

Eq1 <- unlist(jor_pred[j,1]) # Equipo A:  Equipo que juega como Local
                                        # en el encuentro j-esimo.
Eq2 <- unlist(jor_pred[j,2]) # Equipo b: Equipo que juega como Visitante
                                        # en el encuentro j-esimo.


# Hisóricos de partidos jugados entre el equipo A y el equipo B
partido <- dat_trans %>%
            filter(Local == Eq1 & Visitante == Eq2 & Campo == 1)

  if(nrow(partido) != 0){

   trat_datos <- list()

      for(i in 1:nrow(partido)){

      # Aplicamos el tratamiento creado en la función tratamiento_modelo
      # a cada partido en el que se han enfretado el Equipo A y el Equipo B.


        trat_datos[[i]] <- tratamiento_modelo(registro = partido[i,], temp = dat_trans)
      }
   # Transformamos la lista de trat_datos en tibble  com purrr,
   # y lo guardamos en la lista definitiva "encuentros".
   encuentros[[j]] <- map(trat_datos, "dif_reg" )%>%
                      map_dfr(~.x) 

  }
}





names(encuentros) <- paste0(jor_pred$Local,"-", jor_pred$Visitante)
```


```{r echo = FALSE}
cant_reg<-map(encuentros,~nrow(.x)) %>%
            enframe(~.x, value = "n_registros") %>%
            unnest() %>%
            arrange(n_registros) %>%
            select(n_registros)%>%
            group_by(n_registros)%>%
            count(name = "rep")%>%
            arrange(n_registros, desc(rep))


filter(cant_reg, n_registros >=30) %>%
  kable(caption = "Partidos con mas de 30 registros", format = "html", align = c("c","c"))%>%
  kable_styling(bootstrap_options = "striped", full_width = F)


menor_30 <- filter(cant_reg, n_registros < 30) %>%
              group_by()%>%
              summarise(menos_15 = sum(rep))
```
<div style= "color:black;">
Vemos que tenemos muy pocos registros para aplicar un modelo. Y que `r menor_30` de los 110 partidos tienen menos de 30 registros.

No obstante, como esta era la idea inicial, vamos a modelizar los partidos, pero solo los cuales hemos obtenidos más de 30 registros.

Procedemos a introducir  en una lista los partidos con más de 30 registros.
</div>

```{r echo=FALSE, warning=FALSE, message=FALSE}
lgl <- map(encuentros, ~nrow(.x)>30) %>%
        unlist() %>%
        which(.>0) 


par_30 <- list()

for(i in 1:length(lgl))
par_30[[i]] <- pluck(encuentros, names(lgl)[i]) %>%
               select(-Local, -Visitante, - Res_cuant)%>%
               mutate(ResF = parse_factor(ResF),
                      Temporada = parse_number(str_trunc(as.character(Temporada),4,ellipsis = "")))


names(par_30) <- names(lgl)

map(par_30, ~summary(.x))
```




## Construcción modelo


### Random Forest. Clasificación

<div style= "color:black;">
Construimos un modelo **Random Forest** utilizando baging.
</div>
```{r}
set.seed(123456)


metrica_modelo <- function(encuentro){


# rsample

enc_split <- initial_split(encuentro, strata = ResF, prop = 0.60)
enc_train <- training(enc_split)
enc_test <- testing(enc_split)

###############################################################################################################
# previsualisacion.

train_y <- enc_train %>% select(resultado = ResF, PGT =  Dif_PGT) %>%
                        mutate(split = "train")
test_y <- enc_test %>% select(resultado = ResF,  PGT = Dif_PGT) %>%
                        mutate(split = "test")

var_y <- bind_rows(train_y, test_y) 
         

# count de la particiones de la variable a predecir.

enc_count <- var_y %>%
              group_by(split) %>%
              count(resultado) 



# Grafico de la proporción de partidos ganados
  
enc_pre_graf<-  var_y %>%
                  ggplot(aes(x= resultado, y = PGT, color = resultado)) +
                  geom_boxplot()+
                  labs(title = "Resultado / PGT")

pre_pros <- list(enc_count, enc_pre_graf)

################################################################################################################

# validación cruzada
enc_folds <- vfold_cv(enc_train, v = 10,strata = ResF, prop =0.6)

# modelo
enc_tuner <- rand_forest(mtry  = .preds(), trees = tune(), min_n = tune())%>%
            set_engine("randomForest")%>%
            set_mode("classification")

# receta
enc_rec <- recipe(ResF  ~., data = enc_train) %>%
      step_downsample(ResF)%>%
      step_zv(all_numeric()) %>%
      step_center(all_numeric()) %>%  
      step_scale(all_numeric()) 
   
# workflow

enc_wf <- workflow() %>%
          add_recipe(enc_rec)%>%
          add_model(enc_tuner)

# Tuning
enc_result <- enc_wf  %>%
             tune_grid(resamples = enc_folds)
 

enc_best <- enc_result %>%
  select_best(metric = "roc_auc")

enc_wf_final <- enc_wf %>%
     finalize_workflow(enc_best) %>%
     last_fit(split = enc_split)


# métrica

enc_metric <- enc_wf_final %>%
              collect_metrics() 


# importancia de las variables

enc_importancia <- enc_wf %>%
                     finalize_workflow(enc_best) %>%
                     fit(data = enc_train) %>%
                     pull_workflow_fit() %>%
                     vip(geom = "point")
# matric de confunsión

enc_mat_conf<- enc_wf_final %>%
         collect_predictions() %>%
         conf_mat(ResF, .pred_class) 

# curva roc
enc_roc_auc <- enc_wf_final %>%
                 collect_predictions() %>%
                 roc_curve(ResF, .pred_GANA, .pred_PIERDE, .pred_EMPATA) %>%
                 autoplot()

resultados <- list(metrica = enc_metric, importancia = enc_importancia, 
                       mat_conf = enc_mat_conf, roc_auc = enc_roc_auc )


list(pre_pors = pre_pros ,resultados = resultados)

}
```





```{r warning=FALSE, message=FALSE, echo = FALSE}
map(par_30, ~metrica_modelo(.x))
```





<div style= "color:black;">

Vemos que  no se obtienen buenos resultados. Pasaremos a otra estructuración de las variables. Y ver si así, conseguimos lograr mejorar.
</div>
</div>