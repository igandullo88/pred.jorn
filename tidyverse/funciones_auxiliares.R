#######################################################################################
####################### Depura_equipo #################################################
#######################################################################################

# Esta funcién se ha programado para usarla en las variables que contienen nombres de equipos.
# Como por ejemplo, Local y Visitante. 
# En esta función, utilizamos funciones del paquete stringr, para evitar nombres de equipos
# duplicados, así como, simplificar los nombres de los equipos. 

depura_equipo <- function(Equipo){
  
  # Se quitán los acentos
  
  Equipo <- chartr( 'áéíóú', 'aeiou', Equipo)
  
  # Se eliminan las acrónimos y/o parte del nombre para simplificar el nombre.
  # Simpre manteniendo una estructura, en la que se pueda identificar al equipo.
  
  Equipo = str_trim(str_remove(Equipo, "Real|AD|Club|CA|CF|FC|UD|SD|RCD|CD|RC|Deportivo|Club Bilbao|Vallecano|Leonesa|Irun"))
  
  # Se reemplaza los nombres de los equipos para lograr una mejor estructura.
  
  Equipo = str_replace_all(Equipo, c("Espanol|Espanyol" = "Español",
                                      "Ath Bilbao|Atletico de Bilbao" = "Athletic",
                                      "Ath Madrid|Atletico de Madrid|Athletic de Madrid|Atletico Aviacion" = "At.Madrid",
                                      "de La Coruña" = "D.Coruña",
                                      "Gijon"= "Sporting"))
  
  # Quitamos los restante complementos de los nombres que tengan "de" como 
  # por ejemplo "de Gijon".
  
  Equipo = str_split_fixed(Equipo, " de ",2)[,1]
  
  # Se efectúan las últimas modificaciones eliminando y reemplazando los string oportunos.
  
  Equipo = str_remove(Equipo, "Atletico")
  Equipo = str_trim(str_replace_all(Equipo, c("Santander" = "Racing",
                                            "Gimnastico|Gimnàstic" = "Gimnastic")))
  Equipo
  
}
  
# Hay que tener en cuenta que a lo largo de las 88 temporadas son muchos los equipos que
# han cambiado de nombre, o equipos que han desaparecidos. Y también, que cada temporada
# asciende y desciende varios equipos. 
# Produciendo todo esto, una complejidad a la hora de obtener una estructura sálida para
# unificar los datos. Que las solventamos con esta función. 

#_____________________________________________________________________________________#

########################################################################################
########################## ranking #####################################################
########################################################################################

# Con esta función logramos establecer el ranking de los equipos siguiendo los criterios
# de averrage de la 1º vuelta de la temporada.

# La función recibe los datos de la temporada y el número de jornada, en la cual calculamos 
# el ranking de los equipos. 

ranking <- function(df,  jornad){
  # Comenzamos guardando los datos de la jornada que vamos a calcular el ranking.
  df <- df %>% 
    filter(Jornada == jornad) %>%
    select(Local, Visitante, ends_with("Loc"), ends_with("Vis"))
  
  
  # Guardamos los columnas de Local y visitante, con select asignamos el mismo nombre
  # a las columnas, para poder hacer bind_rows.
  
  flocal <- df %>% 
              select(Equipo =  Local, Puntuacion = Punt_Loc , DG = DG_Loc, GF = GF_Loc)
  fvis <- df %>% 
              select(Equipo = Visitante, Puntuacion = Punt_Vis, DG = DG_Vis, GF = GF_Vis)
  
   # Hacemos bind_rows ordenamos con los criterios del average ida.
   # Puntuacion, DG, GF decrecientes. Y en caso de coincidencia en todo. Por nombre equipo
   order = bind_rows(flocal, fvis) %>%
            arrange(desc(Puntuacion),desc(DG),desc(GF),Equipo) %>%
            mutate(rank = 1:(nrow(df)*2)) %>% # Creamos el ranking 
            select(Equipo, rank) # y seleccionamos solo Equipo y rank
   
   # Creamos dos columnas rank_Loc: ranking del equipo local de la jornada x.
   # rank_Vis: ranking del equipo visitante de la jornada x.
   # Para crear estas varibles hacemos inner_join entre df y order, mediante la clave
   # que queramos, en el caso de Local (Local, Equipo), y para visitante (Visitante, Equipo)
   # despues de usar inner_join, seleccionamos solo rank, y convertimos la columna en vector.
   
   bind_cols(rank_Loc =  unlist(inner_join(df, order, by = c("Local" = "Equipo"))%>% 
                                  select(rank)
                                ),  
             rank_Vis = unlist(inner_join(df, order, by = c("Visitante" = "Equipo"))%>% 
                                 select(rank)
                               )  
             )
  
}

#_______________________________________________________________________________#


#######################################################################################
############### ind ###################################################################
#######################################################################################

# Esta función se utiliza en desempate_p_equipos. 
# y es un indice de comparacion dos a dos. Es decir, si son 4 equipos los que empatan.
# habrá que comparar:
#  1-2
#  1-3
#  1-4
#  2-3
#  2-4
#  3-4 
# Pues ese es el resumen, de la funcionalidad, de esta función.

ind = function(n){
  m <- 1
  fila <-sum((n-1):1)
  ind = matrix(0, ncol =2, nrow = fila, byrow = TRUE)
  for( i in 1:(n-1)){
    for(j in 1:n){
      if(j>i){
        
        ind[m,1] <- i
        ind[m,2] <- j
        m = m+1
      }
    }
  }
  colnames(ind) <- c("ind1", "ind2")
  ind <- as_data_frame(ind)
  ind
}

# _____________________________________________________________________________________#

########################################################################################
############################# ranking2 #################################################
########################################################################################


# La función ranking2 sirve es la base de la construcción del averrage para los jornadas
# de los partidos de vuelta. Su principal función es detectar si hay equipos con los mismos
# puntos, si los detecta, busca los encuentros que se han producidos entre ellos a lo largo 
# de la temporada. Y una vez construido el tibble de los encuentros empatados y la lista 
# compuesta de tibbles con los resultados de los encuentros de los equipos empatados, 
# aplica procedimientos de desempate mediante las funciones de desempate.

# Por tanto, ranking2, recibe el tibble de la jornada que vamoa a examinar, los
# resultados de la temporada desde el inicio hasta la jornada axtual. Y l, valor buleano
# que inidica si de aplica el average antiguo o el actual.

ranking2 <- function(df, temporada, l){
  
  # Construimos tibble unificado, como ya vimos en ranking. Pero unificar el nombre
  # de la columnas usamos columnas de stingr.
  bind_Loc <- df %>% select(Local, ends_with("Loc"))
  colnames(bind_Loc) <- c("Equipo",str_remove(colnames(bind_Loc),"_Loc|_Vis")[-1])
  bind_Vis <- df %>% select(Visitante, ends_with("Vis"))
  colnames(bind_Vis) <-c("Equipo",str_remove(colnames(bind_Vis),"_Loc|_Vis")[-1])
  
  
  # Guardamos un tible con los puntos que aparecen más de una vez, lo que es lo mismo, 
  # Guarda un tibble, si hay más de un quipo con los mismos puntos
  Puntos_emp <- bind_rows(bind_Loc, bind_Vis) %>%
    count(Punt) %>%
    filter(n >1) %>%
    select(Punt)
  
  if(nrow(Puntos_emp) !=0){ # Si el número de fila es 0, termina la función.
    
    ######################################################################################
    ##################### Empatados#######################################################
    ######################################################################################
    
    # Guardamos todos los equipos que han empatados a puntos
    empatados <- bind_rows(bind_Loc, bind_Vis) %>% 
      filter(Punt %in% unlist(Puntos_emp))%>%
      # Construimos estas tres variables, que las completaremos con el uso de otras funciones.
      # De aplicación de procedimientos de desempate.
      mutate(Puntos_Des = 0, 
             DG_Des = 0,
             GF_Des = 0) %>%
      arrange(Punt) %>% # Ordenamos por punto, simplemente por visualizar (si nos hiciera falta)
      select(Equipo, Puntos = Punt, DG, GF, Rango = rank, Puntos_Des, DG_Des, GF_Des)
    
    
    
    ##########################################################################################
    ############### resul_empa. lista  de Desempate ##########################################
    ##########################################################################################
    
    # Por ejemplo, supongamos que en la jornada x hay  3 empates. No confundir con 3 equipos.
    # Por cada empate p equipo empatados,  con p >=2.
    # Pues en resul_empa es una lista de longitud igual al número de empate que halla en la jornada
    # y cada lista dentro de resul_empa esta compuesta de los resultados que se hallan producido
    # a lo largo de la jornada por los equipos que están enpatado a esa puntuaciún.
    # Por ej. el Sevilla y el Betis empatan a 32 puntos en la jornada 30, pues la funciún busca
    # cuando Sevilla - Betis o Betis - Sevilla se han jugado hasta la jornada 30 y lo guarda en 
    # en una lista, y así con todos los empates que halla en la jornada.
    # Nótese: Que la función se aplica a partir de la primera jornada de la vuelta, por tanto, 
    # habrá, en cualquiera de los supuesto, como mínimo un resultado.
    
    resul_empa = list()
    i <- 1
    empate= unlist(Puntos_emp)
    q= list()
    
    repeat{
      # Guardamos  los nombres de los i-ésimos equipos empatados a puntos
      q[[i]] = empatados$Equipo[empatados$Puntos == empate[i]]
      
      # número de quipos empatados.
      m = length(q[[i]])
      
      # Usamos la función ind de m. La cual, hemos expliacado más arriba.
      # Para las posibles comparaciones múltiples.
      indice = ind(m)
      
      
      for(j in 1:nrow(indice)){ # bucle de l-ésima comparación
        # La estructura que hemos creado con ind, nos ayuda a capturar todos los datos 
        # de los enfrentamientos que se han producido entre los equipos empatados.
        if(j>1){
          
          # Si j es mayor que 1, seguimos guardando filas correspondiente al j-ésimo 
          # equipo de ind1 y el j-ésimo equipo de ind2
          Eq1 = q[[i]][indice$ind1[j]]
          Eq2 = q[[i]][indice$ind2[j]]
          
          bind   <- temporada %>%
            filter( Local == Eq1 & Visitante == Eq2 | Local == Eq2 & Visitante == Eq1)  
          
          resul_empa[[i]] =  bind_rows(resul_empa[[i]], bind)
        }else{
        
          Eq1 = q[[i]][indice$ind1[j]]
          Eq2 = q[[i]][indice$ind2[j]]
          # Encuentros disputados entre el j-ésimo equipo de ind1 y el j-ésimo equipo
          # de ind2
          resul_empa[[i]] <-  temporada %>%
            filter( Local == Eq1 & Visitante == Eq2 | Local == Eq2 & Visitante == Eq1) 
          
        }
      }
      
      
      if(i  == length(empate) ){
        break
      }
      i <- i+1
    }
    
    # En este bucle aplicamos las funciones de desempate
    # para la reasignación de rango. 
    
    for(t in 1:length(resul_empa)){ # vamos recorriendo el número de empates que tenemos
      
      desempate = resul_empa[[t]] # Guardamos el t-ésimo empate
      Equipos <- unique(c(desempate$Local, desempate$Visitante)) # Y el núm de eq. empatados
      
      
      if(length(Equipos) == 2){# Si la longitud de Equipos es 2
        # Aplicamos la función desempate_2_equipos(Ver función)
        
        cambio = desempate_2_equipos(df, desempate, empatados, l)
        
      }else{
        # En c.c. aplicamos la función desempate_p_equipos(Ver función)
        
        cambio <- desempate_p_equipos(df, desempate, empatados)
      }
      
      
      for(h in 1:length(Equipos)){ 
        
        # Aplicamos a df, recordemos que es el tibble que recible la función
        # ranking2, los cambios que se hallan producios en las funciones de desempate.
        
        if(sum(df$Local == cambio$Equipo[h]) == 1){
          
          df$rank_Loc[df$Local == cambio$Equipo[h]] = cambio$Rango[h]
          
        }else{
          df$rank_Vis[df$Visitante == cambio$Equipo[h]] = cambio$Rango[h]
        }
        
      } 
    }
    
    # Sacamos los rangos que hemos cambiado.
    df %>% select(rank_Loc, rank_Vis) 
    
  }else{
    df %>% select(rank_Loc, rank_Vis)
  } 
}
# _____________________________________________________________________________________#



#################################################################################
################ desempate_2_equipos ############################################
#################################################################################


# Con la función desempate_2_equipos logramos modificar, si procede,
# el ranking de dos equipos que están empatados.

# La función recibe los siguientes atributos:
# - df: tibble de la jornada x
# - desempate: resultado de los partidos disputados de los dos equipos a los que se le 
# aplica la función
# - emapatados: equipos empatados de la jornada x.
# - l: Si se aplica el averrage nuevo, o el antiguo.


desempate_2_equipos <- function(df, desempate, empatados, l){
  
  # Si desempate tiene solo una fila. Se aplica la función ranking. Si es mayor que 1
  # es porque ya se han jugado los dos encuentros. Y por tanto, se aplica los resultados
  # de average que dicta el reglamento.
  
  if(nrow(desempate) > 1){  
    
    if(nrow(desempate > 2)){
      # Esta condición tan solo se da en la temporada 1986/1987. La temporada más larga de
      # la historia. En esta temporada hubo equipos que se enfretaron hasta en tres ocaciones,
      # Dos en la vuelta y uno en la ida. Lo que hacemos, antes este contratiempo, 
      # es sumar los goles de los dos encuentros disputados en la vuelta, lo guardamos en una
      # variable, a la que llamamos reemplazar.
      # Quedarnos solo con el partido de ida y el último de vuelta. Y en el último de vuelta
      # reemplazamos los goles de ambos equipos, los cuales hemos guardado en reemplazar.
      reemplazar<- desempate %>% filter( Campo == 1) %>%
        mutate(Gol_Loc = sum(Gol_Loc),
               Gol_Vis = sum(Gol_Vis)) %>%
        filter(Fecha == max(Fecha))
      desempate= desempate %>% 
        filter(Fecha == min(Fecha) | Fecha == max(Fecha)) %>%
        mutate(Gol_Loc = c(Gol_Loc[1],reemplazar$Gol_Loc), 
               Gol_Vis = c(Gol_Vis[1], reemplazar$Gol_Vis)
        )
      
    }
    
    # Para lograr desempetar. Necesitamos los resulados del partido de ida y de vuelta
    # estructurado de la siguiente manera:
    # - Hacemos left_join de desemapate y empatados by(Local = Equipo), por que
    # necesitamos la variable Rango de empatados, y es posible hacer el join con
    # desempate por la variable Local, ya que cada uno de los dos equipos juego solo
    # una vez como Local.
    # - Suma de los goles marcados en la ida, y en la vuelta.
    # - Diferencia de los goles marcados en la ida y en la vuelta.
    # - GFV: Goles marcados como visitante de cada equipo.
    # - Reestructuramos a columnas los partidos de ida y vuelta. 
    
    des = desempate %>% select(-Jornada) %>% 
      left_join(empatados, by = c("Local" = "Equipo")) %>%
      mutate(Equipo = Local,
             Suma = Gol_Loc + Gol_Vis,
             Dif = Gol_Loc[2:1] - Gol_Vis[2:1],
             GFV = c(Gol_Vis[2], Gol_Vis[1]),
             Ida = c(Gol_Loc[1], Gol_Vis[1]),
             Vuelta = c( Gol_Vis[2], Gol_Loc[2])
      )%>% 
      select(Equipo, Ida, Vuelta, Suma, Dif, GFV, Rango) # Seleccionamos las variables deseadas.
    
    ida <- des$Ida # Guardamos el vector de resultado de ida
    vue <- des$Vuelta # y de vuelta
    
    
    # ______________________________________________________________________________________#
    # La estructura del tiblle definido como des, a priori, es compleja de entender. Pero, 
    # ha sido la forma más sencilla de ordernar, utilizando arrange y de ahorrar condicionales.
    # ______________________________________________________________________________________#
    
    ########################################################################################
    ############## Condiciones de average ##################################################
    ########################################################################################
    
    ### Average antiguo (hasta la temporada 1995-96) ###
    ## Los criterios del average antiguo solo los mismos que los nuevos exceptuando 
    ## la siguiente condición: 
    # - Si los equipos local y visitante marcan la misma cantidad de goles en los 
    #   dos encuentros, es decir, por ej 3-2 (ida) y 1-0 (vuelta). 
    #   Se aplican las condiciones de la función ranking
    ## Para el resto de situaciones el desempate se hace igual que con el average nuevo.
    
    
    if(l == 0 & (ida[1]+vue[1])== (ida[2]+vue[2])){# Excepción average nuevo
      select(des, Equipo, Rango)
    }else{
      
      # Nota: Si no pasa las condiciones se queda con el ranking, ya establecido por la función
      # ranking. 
      
      # Por ej, con esta condición, si el resultado de la ida es exactamente igual al de la vuelta
      # como puede ser 1-1 y 1-1. No pasa la condición, se queda con el rango asignado.
      
      # Nota. Para explicar estas condiciones. Cuando hablamos de equipo Local, nos referimos 
      # al equipo que empieza como local  en el partido de ida, y cuando hablamos de equipo 
      # visitante, nos referimos al equipo  que empieza como visitante en el partido ida.  
      # Usamos esos términos, aunque nos estemos referiendo al partido de vuelta.
      
      if( sum(c(ida, vue)%%4) !=  0){ 
        
        if(ida[1]== ida[2]){# Si empatan en la ida
          
          # Ordenamos por el resultado de la vuelta, el máximo de goles como visitnate,
          # el campo donde se halla marcado mas goles (ida o vuelta), y si se empatarán
          # en todos los casos, se ordenará por el  rango establecido en ranking.
          
          arrange(des, desc(Vuelta), desc(GFV), desc(Suma), Rango) %>%
            select(Equipo, Rango) %>%
            #  Se cambia el rango si procede.
            mutate(Rango = if(Rango[1] == min(Rango)){Rango}else{Rango +c(-1,1)})
        }else{
          
          if(ida[1] > ida[2]){ # Si en la ida gana o empata el equipo Local
            if(vue[1] >= vue[2]){  # Si en la vuelta gana el equipo Local
              # Gana equipo1
              select(des, Equipo, Rango) %>%
                # Se procede al cambio si procede
                mutate(Rango = if(Rango[1] == min(Rango)){Rango}else{Rango +c(-1,1)} )
            }else{
              # Si en la vuelta gana el equipo visitante.
              # se ordena por la mayor difenrecia de goles, quien a marcao mas goles de vis,
              # y si empatan en todo, por el rango.
              arrange(des, Dif, desc(GFV), Rango)%>%
                select(Equipo, Rango)%>%
                mutate(Rango = if(Rango[1] == min(Rango)){Rango}else{Rango +c(-1,1)} )
            }
          }else{ # En la ida ha ganado el equipo visitante
            
            if(vue[1] <= vue[2]){ # en la vuelta gana o empata el equipo visitante
              #Gana equipo2
              select(des, Equipo, Rango) %>%
                mutate(Rango = if(Rango[2] == min(Rango)){Rango}else{Rango +c(1,-1)} )
            }else{
              # Si gana en la vuelta gana el equipo local, 
              # se ordena por la mayor difenrecia de goles, quien a marcao mas goles de vis,
              # y si empatan en todo, por el rango.
              arrange(des, desc(Dif), desc(GFV), Rango)%>%
                select(Equipo, Rango)%>%
                mutate(Rango = if(Rango[1] == min(Rango)){Rango}else{Rango +c(-1,1)} )
            }
          }
        }
      }
    }
  }         
}



# ____________________________________________________________________________________#


########################################################################################
################## desempate_p_equipos #################################################
########################################################################################

# En esta función logramos el desempate de p equipos. Con p >2.
# Para ello, aplicamos las normas de average dictadas en el reglamento de la LFP.

# Como en desempate, tenemos los resultados con compete a los p equipos implicados.
# Construimos las variables que necesitamos de esos partidos, para poder aplicar arrange.
# Y así, si procede, ordenar el ranking.
# Las variables que necesitamos son referente a los partidos disputados entre los equipos,
# y son:
#- Puntos Loc y Vis
# - GF Loc y Vis
# - GC Loc y Vis
# - DG loc y Vis


desempate_p_equipos <- function(df, desempate, empatados){
  desempate = desempate %>%
    mutate(Punt_Loc = ifelse(Gol_Loc>Gol_Vis,3,ifelse(Gol_Loc == Gol_Vis,1,0)),
           Punt_Vis = ifelse(Gol_Loc>Gol_Vis,0,ifelse(Gol_Loc == Gol_Vis,1,3)),
           GF_Loc = Gol_Loc,
           GF_Vis = Gol_Vis,
           GC_Loc = Gol_Vis,
           GC_Vis = Gol_Loc, 
           DG_Loc = Gol_Loc - Gol_Vis,
           DG_Vis = Gol_Vis - Gol_Loc)
  
  # Para saber cuantos equipos están implicados, caprutamos el nombre de los equipos
  name_eq =  unique(c(desempate$Local, desempate$Visitante))
  
  
  # Hacemos un bucle por equipo implicado, para sumar los puntos, GF y DG adquirido
  # en todos los encuentros disputados con el resto de equipos empatados.
  for(s in 1:length(name_eq)){
    # Guardamos los resultados del s-ésimo equipo empatado
    des<- desempate %>% 
      filter( Local == name_eq [s] | Visitante == name_eq [s]) 
    
    # Vamos capturando las variables acumulativas (puntos, DG, GF) de los partidos de 
    # desempate para el equipo s-ésimo.
    dg_des =0 
    punt_des = 0
    GF_des = 0
    for(p in 1:nrow(des)){
      # Si el equipo s-ésimo es Local sumamos las variables de las columnas correspondiente
      # de Loc
      if(des$Local[p] == name_eq[s]){
        dg_des = dg_des + des$DG_Loc[p]
        punt_des = punt_des + des$Punt_Loc[p]
        GF_des = GF_des + des$Gol_Loc[p]
      }else{ # viseversa si Vis
        dg_des = dg_des + des$DG_Vis[p]
        punt_des = punt_des + des$Punt_Vis[p]
        GF_des = GF_des + des$Gol_Vis[p]
      }
    } 
    
    # Rellenamos las columnas DG_Des, Puntos_Des, GF_Des del tibble empatados, con 
    # los resultados que hemos conseguidos en el anterior bucle.
    empatados$DG_Des[empatados$Equipo == name_eq[s]] = dg_des
    empatados$Puntos_Des[empatados$Equipo == name_eq[s]] = punt_des
    empatados$GF_Des[empatados$Equipo == name_eq[s]] = GF_des
  }
  # Creamos el tibble empata_p conpuesto por los registo de los p equipos empatados
  empata_p = empatados %>% 
    filter( Equipo %in% name_eq)
  
  # El desempate con el average para mas de dos equipos empatados es:
  # ordenar bajo los criterios:
  # - Puntos_Des: el que mayor número de puntos a obtenido en la suma de todos los encuentros.
  # - DG_Des: el que mayor diferencia de goles tenga en la sumatoria de los encuentros.
  # - GF_Des: el mayor goles a favor tenga en la sumatoria de los encuentros.
  # Si el empate continua, se aplican los criterios de ranking.
  empata_p_orden = arrange(empata_p, desc(Puntos_Des), desc(DG_Des),desc(GF_Des), Rango)
  # campturamos rango mínimo y máximo y reasignamos rango en empata_p
  r_min <- min(empata_p_orden$Rango)
  r_max <- max(empata_p_orden$Rango)
  empata_p$Rango = r_min:r_max
  
  empata_p  %>% # salida del equipo y el rango
    select(Equipo, Rango)
}


# _____________________________________________________________________________________#


############################# FIN TRATAMIENTO 1 ############################################


##############################################################################################
##############################################################################################
##################### FUNCIONES TRATAMIENTO 2 ################################################
##############################################################################################
##############################################################################################


#### Var anterior ####

var_anterior <- function(df, Eq, var, jorn_2){
  
  df_Loc <- df %>%
        filter(Local == Eq) %>%
        select(str_c(var,"_Loc"))
  
  df_Vis <- df %>%
            filter(Visitante == Eq ) %>%
            select(str_c(var,"_Vis"))
  
  
  
  if(nrow(df_Loc)==0 & nrow(df_Vis)==0){
    
    df_Loc2 <- jorn_2 %>%
      filter(Local == Eq) %>%
      select(str_c(var,"_Loc"))
    
    df_Vis2 <- jorn_2 %>%
      filter(Visitante == Eq ) %>%
      select(str_c(var,"_Vis"))
    
    ifelse(nrow(df_Loc2) == 0, df_Vis2, df_Loc2)
    
  }else{
    ifelse(nrow(df_Loc) == 0, df_Vis, df_Loc)
  }
  
  
}


agrupa_clasificacion <- function(x){
  if_else(x ==1, 1,
          ifelse(x>1 & x<7, 2,
                 ifelse(x>4 & x<7, 3,
                        ifelse(x>6 & x <18, 4,  5)
                 )
          )
  )
}


# En la función tratamiento_modelo preparamos las variables que  vamos a introducir en el
# modelo. Partiendo de los csv jor_pred.csv (partidos a predecir) y el de registro.csv
# (historico de partidos disputados en la liga). 

# La función recibe:
    # - Eq1: Equipo que juega como local en el encuentro a predecir
    # - Eq2: Equipo que juega como visitante en el encuetor a predecir.
    # - temporada: temporada x completa donde existe un enfrentamiento entre Eq1 y Eq2
    # - registro: encuentro entre Eq1 y Eq2 de la temporada x.

tratamiento_modelo <- function(registro, temp, B365 = FALSE){
  
  ##############################################################################
  #################### BLOQUE TOTAL ############################################
  ##############################################################################
  
  Eq1 = registro$Local
  Eq2 = registro$Visitante
  
  temporada = temp %>%
              filter(Temporada == registro$Temporada)
  
  jorn_ant <- temporada %>%
              filter(Jornada == registro$Jornada -1)
  
  jorn_ant2 <- temporada %>%
               filter(Jornada == registro$Jornada -2)
  
  # Diferencia de puntos
  
  Punt_Loc <- unlist(var_anterior(jorn_ant,Eq1, "Punt",  jorn_2 = jorn_ant2)) %>% unlist()
  Punt_Vis <- unlist(var_anterior(jorn_ant,  Eq2,  "Punt", jorn_2 = jorn_ant2))  %>% unlist()
  
  Dif_Pto <- Punt_Loc - Punt_Vis
  
  Pto <-  bind_cols(Punt_Loc = Punt_Loc, Punt_Vis  = Punt_Vis)
  
  # Diferencia ranking (posición)
  
     rank_ant_Loc <- var_anterior(jorn_ant,Eq1, "rank", jorn_2 = jorn_ant2) %>% unlist()
     rank_ant_Vis <- var_anterior(jorn_ant, Eq2, "rank",jorn_2 = jorn_ant2) %>% unlist()
     
     Dif_rank <- rank_ant_Loc - rank_ant_Vis
     
     # Ambas columnas
     rank_ant <- bind_cols(rank_ant_Loc = rank_ant_Loc, rank_ant_Vis = rank_ant_Vis) 
    
  
  # Columnas "Diferencia proporción Partidos ganados totales"
  
 
  PGT_Loc <- unlist(var_anterior(jorn_ant,Eq1, "PG",  jorn_2 = jorn_ant2)) / jorn_ant$Jornada[1]
  PGT_Vis <- unlist(var_anterior(jorn_ant,  Eq2,  "PG", jorn_2 = jorn_ant2)) / jorn_ant$Jornada [1]
  
  Dif_PGT <- round(PGT_Loc - PGT_Vis,2)
  
  # Ambas columnas
  PGT <- bind_cols(PGT_Loc = PGT_Loc, PGT_Vis = PGT_Vis) %>% round(2)
  
 
  
  # Columna "Diferencia del promedio goles a favor totales" 
  
  GFT_Loc <- unlist(var_anterior(jorn_ant,Eq1, "GF", jorn_2 = jorn_ant2)) / jorn_ant$Jornada[1]
  GFT_Vis <- unlist(var_anterior(jorn_ant,  Eq2,  "GF", jorn_2 = jorn_ant2)) / jorn_ant$Jornada[1]
  
  Dif_GFT <-  round(GFT_Loc - GFT_Vis,2)
  
  # Ambas columnas
  GFT <- bind_cols(GFT_Loc = GFT_Loc, GFT_Vis = GFT_Vis) %>% round(2)
  
  # Columnas "Diferencia del promedio goles encontra totales"
  GCT_Loc <- unlist(var_anterior(jorn_ant,Eq1, "GC", jorn_2 = jorn_ant2)) / jorn_ant$Jornada[1]
  GCT_Vis <- unlist(var_anterior(jorn_ant,  Eq2,  "GC", jorn_2 = jorn_ant2)) / jorn_ant$Jornada[1]
  
  Dif_GCT <- round(GCT_Loc - GCT_Vis,2)
  
  # Ambas columnas
  GCT <- bind_cols(GCT_Loc = GCT_Loc, GCT_Vis = GCT_Vis) %>% round(2)
  
  
  
  # Columna  "racha de victoria de los últimos 5 partidos del equipo local".
  
  racha_Loc <- temporada %>%
    filter( Local == Eq1  | Visitante == Eq1) %>%
    filter(Jornada < registro$Jornada & Jornada >= registro$Jornada - 10) %>%
    mutate(racha = ifelse(Local == Eq1,
                          ifelse(Gol_Loc > Gol_Vis, 3, ifelse(Gol_Loc == Gol_Vis,1 ,-1)),
                          ifelse(Gol_Loc < Gol_Vis, 3, ifelse(Gol_Loc == Gol_Vis,1 ,-1)))
    ) %>%
    summarise(racha = sum(racha))
  
  racha_Vis <- temporada %>%
    filter( Local == Eq2  | Visitante == Eq2) %>%
    filter(Jornada < registro$Jornada & Jornada >= registro$Jornada - 10) %>%
    mutate(racha = ifelse(Local == Eq2,
                          ifelse(Gol_Loc > Gol_Vis, 3, ifelse(Gol_Loc == Gol_Vis,1 ,-1)),
                          ifelse(Gol_Loc < Gol_Vis, 3, ifelse(Gol_Loc == Gol_Vis,1 ,-1)))
    ) %>%
    summarise(racha = sum(racha))
  
  
  Dif_racha <- racha_Loc- racha_Vis
  
  # Ambas columnas
  racha <- bind_cols(racha_Loc = racha_Loc$racha, racha_Vis = racha_Vis$racha)
  
  
  ##################################################################################
  ##################  BLOQUE CASA/FUERA ############################################
  ##################################################################################
  
  
  # Columna "PG local en casa"
  PG_Loc_C <- temporada %>%
    filter( Local == Eq1 & Jornada < registro$Jornada) %>%
    mutate(G_casa = ifelse(Gol_Loc > Gol_Vis,1,0))%>%
    summarise(PG_CT = sum(G_casa, na.rm = T) / nrow(.))
  
  # Columna "PG visitante fuera de casa"
  PG_Vis_F <- temporada %>%
    filter( Visitante == Eq2 & Jornada < registro$Jornada) %>%
    mutate(G_Fuera = ifelse(Gol_Loc < Gol_Vis,1,0))%>%
    summarise(PG_FCT = sum(G_Fuera, na.rm = T) / nrow(.))
  
  # Ambas columnas
  PG_CF <- bind_cols(PG_Loc_C, PG_Vis_F)
  
  # Columna "Diferencia de la prporción de partidos ganados CASA/FUERA"
  Dif_PG_CF <- PG_Loc_C - PG_Vis_F
  
  
  # Columna "Promedio de goles a favor del equipo local en casa"
  GF_Loc_C <- temporada %>%
    filter( Local == Eq1 & Jornada < registro$Jornada)%>%
    summarise(GF_C=  sum(Gol_Loc, na.rm = T) /nrow(.))
  
  # Columna "Promedio de goles a favor del eq.  visitante fuera de casa"
  
  GF_Vis_F <- temporada %>%
    filter( Visitante == Eq2 & Jornada < registro$Jornada)%>%
    summarise(GF_FC= sum(Gol_Vis, na.rm=T) / nrow(.))
  
  # Ambas columnas
  GF_CF <- bind_cols(GF_Loc_C, GF_Vis_F)
  
  
  # Columna "Diferencia del promedio de goles a favor CASA/FUERA"
  Dif_GF_CF <- GF_Loc_C - GF_Vis_F
  
  
  
  # Columna "Promedio de goles en contra del equipo local en casa"
  GC_Loc_C <- temporada %>%
    filter(Local == Eq1 & Jornada < registro$Jornada)%>%
    summarise(GC_C= sum(Gol_Vis, na.rm = T) /nrow(.))
  
  # # Columna "Promedio de goles en contra del eq.  visitante fuera de casa"
  GC_Vis_F <- temporada %>%
    filter( Visitante == Eq2 & Jornada < registro$Jornada)%>%
    summarise(GC_FC =  sum(Gol_Loc, na.rm = T) / nrow(.))
  
  # Ambas columnas
  GC_CF <- bind_cols(GC_Loc_C, GC_Vis_F)
  
  # Columna "Diferencia del promedio de goles en contra CASA/FUERA"
  Dif_GC_CF <- GC_Loc_C - GC_Vis_F 
  
  
  # Racha de victoria, en los últimos 5 partidos,  en casa del equipo local
  
  racha_Loc_C <- temporada %>%
    filter( Local == Eq1 & Jornada < registro$Jornada) %>%
    arrange(desc(Jornada)) %>%
    filter( Jornada >= ifelse(nrow(.)<11, 
                              nth(Jornada, nrow(.)),
                              nth(Jornada, 10) 
                              ) 
            )%>%
    mutate(racha = ifelse(Gol_Loc > Gol_Vis, 3, ifelse(Gol_Loc == Gol_Vis,1 ,-1))) %>%
    summarise(racha = sum(racha, na.rm = T))
  
  # Racha de victoria, en los últimos 5 partidos,  fuera de casa del equipo visitante
  racha_Vis_F <- temporada %>%
    filter( Visitante == Eq2 & Jornada < registro$Jornada) %>%
    arrange(desc(Jornada)) %>%
    filter( Jornada >= ifelse(nrow(.)<11, 
                              nth(Jornada, nrow(.)),
                              nth(Jornada, 10) 
                              ) 
            ) %>%
    mutate(racha = ifelse(Gol_Loc > Gol_Vis, -1, ifelse(Gol_Loc == Gol_Vis,1 ,3))) %>%
    summarise(racha = sum(racha,na.rm = T))
  
  # Ambas columnas
  racha_CF <- bind_cols(racha_Loc_C = racha_Loc_C$racha, racha_Vis_F =  racha_Vis_F$racha)
  
  # Diferencia de rachas de victoria CASA/fUERA
  
  Dif_racha_CF <- racha_Loc_C - racha_Vis_F 
  
  
  
  ########################################################
  
  #### Preparación de la salida ####
  if(B365 == "TRUE"){
  
  
  registro <- registro %>%
    mutate( 
           Res_cuant = ifelse(is.numeric(Gol_Loc),Gol_Loc - Gol_Vis, "NA"),
           ResF = ifelse(is.numeric(Gol_Loc),
                    ifelse(Gol_Loc - Gol_Vis >0, "GANA", 
                      ifelse(Gol_Loc - Gol_Vis <0, "PIERDE", "EMPATA")), 
                    "NA"),
           Gr_Loc = agrupa_clasificacion(rank_ant_Loc),
           Gr_Vis = agrupa_clasificacion(rank_ant_Vis),
           Dif_Gr = Gr_Loc - Gr_Vis,
           BET365_Loc = 100/ as.numeric(BET365_Loc),
           BET365_Vis  = 100/as.numeric(BET365_Vis),
           BET365_Em = 100/as.numeric(BET365_Em),
    ) %>%
    select(Temporada,Jornada, starts_with("Res"), Dia, Mes, starts_with("Gr"),
           starts_with("BET"))
  }else{
    registro <- registro %>%
      mutate( 
        Res_cuant = ifelse(is.numeric(Gol_Loc),Gol_Loc - Gol_Vis, "NA"),
        ResF = ifelse(is.numeric(Gol_Loc),
                      ifelse(Gol_Loc - Gol_Vis >0, "GANA", 
                             ifelse(Gol_Loc - Gol_Vis <0, "PIERDE", "EMPATA")), 
                      "NA"),
      ) %>%
      select(Temporada,Jornada, starts_with("Res"), Dia, Mes, Local, Visitante)
    
  }
  
  registro_dif <- bind_cols( registro,
                    Dif_rank = unlist(Dif_rank),
                    Dif_Pto = unlist(Dif_Pto),
                    Dif_PGT = Dif_PGT,
                    Dif_GFT = Dif_GFT,
                    Dif_GCT = Dif_GCT,
                    Dif_racha = unlist(Dif_racha),
                    Dif_PG_CF = unlist(Dif_PG_CF),
                    Dif_GF_CF = unlist(Dif_GF_CF),
                    Dif_GC_CF = unlist(Dif_GC_CF),
                    Dif_racha_CF = unlist(Dif_racha_CF)
                  ) %>%
                  as_tibble()%>%
                  select(-starts_with("Gr"), -starts_with("Punt"))
  
  registro <- bind_cols(registro, Pto, rank_ant, PGT,GFT,GCT,racha,PG_CF,GF_CF,GC_CF,racha_CF)%>%
              select(-starts_with("Diff"))
  
  
  # La función devuelve un tibble con las variables que vamos a introducir en el modelo  
  
  
     list( dif_reg = registro_dif, registro_tot = registro)
   
}
