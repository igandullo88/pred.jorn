---
title: "<h1 class= 'titulo'> Manipulacion y ordenacion de datos </h1>"
output: 
  html_document:
    css: salida-html/estilo.css
    highlight:  espresso
    theme: darkly
knit: (function(inputFile, encoding) { 
         out_dir <- 'salida-html';
         rmarkdown::render(inputFile,
                           encoding=encoding, 
                           output_file=file.path(dirname(inputFile), out_dir, 'Imp-ord.html')) })
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, class.source="colum_col", class.output = "salcolor",
                      warning =FALSE, message = FALSE)
```


# {.div_rmd}

Librerías utilizadas 
```{r warning=FALSE, message=FALSE}
library(tidyverse)
library(lubridate)
library(magrittr)
library(jsonlite)
```

Cargamos las funciones fabricadas.

```{r}
source("funciones_auxiliares.R", encoding = "UTF-8")
```

Recogemos un vector la ruta de las 89 temporadas. 

```{r}
carpeta <- str_c(seq(1920,2010,10),"s")
carpeta <- rep(carpeta,  c(2,7,rep(10,8)))
subcarpeta <- c(str_c(1928:1998,"-", 29:99)[-(9:11)],
               str_c(1999:2019, "-", rep(c(0,""), c(10,11)) ,0:20))
ruta <- str_c("../repositorio/",carpeta,"/", subcarpeta, "/es.1.csv", sep = "")
```


Al efectuar el tratamiento de los datos obtenemos, por temporada, un tibble con las sigueintes variables:

- **Temporada**: Temporada que se esta jugando. 

- **Dia**: Día de la semana, en la que se juega el encuentro

- **Mes**: Mes en el que se juega en el partido.

- **Fecha**: Fecha en formato `date` en la que se disputa el partido.

- **Local** y **Visitante**. Equipos que se enfretan en esa jornada.

- **Gol_Loc** y **Gol_Vis**. Goles anotados al terminar el encunetro.

- **GF_Loc** y **GF_Vis**. Goles a favor acumulados.

- **Gc_Loc** y **GC_Vis**. Goles en contra acumulados.

- **DG_Loc** y **DG_Vis**. Diferencia de goles acumulada (GF-GC).

- **Punt_Loc** y **Punt_Vis**. Puntuacion acuumulada.

- **rank_Loc** y **rank_Vis**. Puesto que ocupa en la tabla al temrminar la jornada.
 
- **PG_Loc** y **PP_Vis**. Partidos ganados acumulados.

- **PE_Loc** y **PE_Vis**. Partidos empatados acumulados.

- **PP_Loc** y **PP_Vis**. Partidos perdidos acumulados.

Estas variables las obtenemos de transformaciones de las columnass importadas de los archivos csv del repositorio. Todas las transformaciones y funciones aplicadas se explican en los comentarios.

```{r warning = FALSE, message=FALSE}
# En este chunk hacemos un bucle efectuando las transformaciones oportunas por temporada, 
# y guardamos el conjunto obtenido en una lista. Por tanto, al finalizar el bucle tendremos
# una lista, compuesta de n listas(tibbles). Donde n, es el número de temporadas disponibles.


n_temporadas = length(ruta)  # En ruta, tenemos el número de temporadas disponibles.
TEMPORADAS = list() # Creamos una lista, donde introduciremos los datos de las temporadas.


for(l in 1:n_temporadas){ # Iniciamos el bucle
  

# Importamos el archivo csv de la temporada l-ésima.
  
datos <- read_csv(ruta[l], 
                  col_names = c("Fecha", "Local", "Resultado", "Visitante"),
                  col_types = "-ccc-c",
                  skip = 1)

# Como vimos en el documento Historia.html el puntuaje cambia a partir de la temporada
# 1995-96 cambaia de dos puntos a tres puntos. 

punt_gan = ifelse(l >= which(subcarpeta == "1995-96") ,3, 2) 

# También vimos, que la forma de desempatar también cambia en esta temporada.

aver_antig = ifelse(l >= which(subcarpeta == "1995-96") ,1, 0) # Guardamos, en valor lógico
                                                               # y lo aplicaremos más adelante.

n = nrow(datos) # Tamaño de la muestra.

#################################################################################
####################### Tansformaciones de las variables#########################
#################################################################################
 datos %<>% 
     mutate(
         # Sacamos la jornada de la var. Local 
         Jornada = str_split_fixed(datos$Local, "[\\(\\)]",3)[,2],
         # Convertimos Jornada en var. de tipo numérico.
         Jornada = parse_number(Jornada),
         # Capturamos el nombre del equipo Local sin la jornada
         Local = str_split_fixed(datos$Local, "[\\(\\)]",3)[,1],
         # idem visitante
         Visitante = str_split_fixed(datos$Visitante, "[\\(\\)]",3)[,1],
         # Aplicamos funcion depura_equipo a la var. Local y Visitante
         Local = depura_equipo(Local),
         Visitante = depura_equipo(Visitante),
         # Extraemos la Fecha, le quitamos el dia.
         Fecha = str_split_fixed(datos$Fecha, "[\\(\\)]",4)[,3],
         # Convertimos la var. Fecha a formato date.
         Fecha = dmy(Fecha),
         # Creamos una var. Dia con el dia en que se juega el partido
         Dia = wday(Fecha),
         # Creamos una var. Mes con el mes en el que se juega el encuentro
         Mes = month(Fecha),
         # Creamos una var. Gol_loc con los goles del Equipo Local
         # Resultado lo dividimos en dos por '-' y nos quedamos con la col 1
         Gol_Loc = str_split_fixed(datos$Resultado, "-", n=2)[,1],
         # Creamos una var. Gol_Vis con los goles del Equipo Visitante
         # Resultado lo dividimos en dos por '-' y nos quedamos con la col 2
         Gol_Vis = str_split_fixed(datos$Resultado, "-", n=2)[,2],
         # Convertimos Gol_Loc y Gol_Vis en formato numérico
         Gol_Loc = parse_number(Gol_Loc),
         Gol_Vis = parse_number(Gol_Vis),
         # Creamos dos nuevas variables Punt_Loc y Punt_Vis. En la cual
         # Para Punt_Loc, si Gol_Loc > Gol_Vis se le asigna valor de la variabel punt_gan,
         #  si son  iguales un 1,  y en c.c. un 0.
         # Punt_Vis sigue el mismo procedimiento.
         Punt_Loc = ifelse(Gol_Loc>Gol_Vis,punt_gan,ifelse(Gol_Loc<Gol_Vis,0,1)),
         Punt_Vis = ifelse(Gol_Loc>Gol_Vis,0, ifelse(Gol_Loc<Gol_Vis,punt_gan,1)),
         # Los variable GF, GC, DG, PG, PE, PP Se definen como numéricas,
         # y se completarán más adelante.
         GF_Loc = numeric(n), GF_Vis = numeric(n), 
         GC_Loc = numeric(n), GC_Vis = numeric(n),
         DG_Loc = numeric(n), DG_Vis = numeric(n),
         rank_Loc = numeric(n), rank_Vis = numeric(n), 
         PG_Loc = numeric(n),  PE_Loc = numeric(n), PP_Loc = numeric(n), 
         PG_Vis = numeric(n),   PE_Vis = numeric(n), PP_Vis = numeric(n),
         # La variable ID, contiene los números de 1:n.
         ID = 1:n
         ) %>%
   # Se seleccionan las variables en el orden deseado.
   select(ID, Jornada, Dia, Mes, Fecha, Local, Visitante, starts_with("Gol"),      
       starts_with("Punt"),starts_with("rank"), starts_with("GF"),
       starts_with("GC"), starts_with("DG"),
       starts_with("PG"), starts_with("PP"), 
       starts_with("PE"))


n_jornadas <- max(unique(datos$Jornada)) # numero de jornadas que se disputan
Equipos <- unique(datos$Local) # Equipos que juegan la temporada

# Se define la variable Campo. Donde se define si la jornada en la que se juega
# corresponde a la primera vuelta de la temporada, o a la segunda. Se define como 0 ó 1 
# respectivamente.

datos %<>%
   mutate(Campo = ifelse(Jornada < length(Equipos) -1, 0, 1)) #Ida y Vuelta
# ________________________________________________________________________________#

###################################################################################
########## Obtenención de las variables acumulativas ##############################
###################################################################################



j <- 1
repeat{ 

# En este bucle se asigna los valores Punt, GF, GC,DG,PG, PE y PP  a cada equipo  
  
# 1º Guardamos en las var Loc las var. Puntos  = puntos del equipo local
  # Gol_fav  = Goles a favor del equipo local
  # Gol_enc = Goles encontra del equipo local = Goles a favor del eq. visitante
Loc <- datos %>% filter(Local == Equipos[j]) %>% 
   select(ID,Jornada, Puntos = Punt_Loc, Gol_fav= Gol_Loc, Gol_enc = Gol_Vis) 
# 2º adaptamos el proceso para el equipo visitnate.
Vis <- datos %>% filter(Visitante == Equipos[j]) %>% 
  select(ID, Jornada,Puntos = Punt_Vis,Gol_fav = Gol_Vis, Gol_enc = Gol_Loc)

# 3º Creamos un tibble de la  unificanción, con bind_row, de Loc y Vis
df_equipo <- bind_rows(Loc, Vis) %>%
               as_tibble()%>%
               arrange(ID) # ordenamos por ID.

# 4º Creamos los valores acumulados de las variables puntos, goles a favor,
# goles en contra, y las de partidos ganados, empatados y perdidos.
df_equipo<- df_equipo %>%
             mutate(PT = cumsum(df_equipo$Puntos), # puntos
                    GF = cumsum(df_equipo$Gol_fav), # goles a favor
                    GC = cumsum(df_equipo$Gol_enc), # goles en contra
                    PG = cumsum(ifelse(df_equipo$Puntos ==punt_gan,1,0)), # partidos ganados
                    PE = cumsum(ifelse(df_equipo$Puntos ==1,1,0)), # partidos empatados
                    PP = cumsum(ifelse(df_equipo$Puntos ==0,1,0)) # partidos perdidos
                  )






i <- 1

repeat{
  
  
  # En df_equipo tenemos el data.frame del j-ésimo equipo con los datos guardados
  # de toda la temporada.
  
  # Lo que hacemos en este bucle, es recorrer cada jornada y  vamos exportando los datos de 
  # df_equipo en nuestro tibble de inicio. Para ello, sabemos que cada ID de df_equipo
  # corresponde a la jornafa i-ésimo del equipo j-ésimo, Por tanto el equipo se encontrará 
  # en esa jornada en datos$Local o datos$Visitante de nuestro tibble "datos". 
  # Estableciendo esta condición, ya tan solo tenemos que introducir los datos.
  
  if(datos$Local[df_equipo$ID[i]] == Equipos[j]){
    #  datos$Local[df_equipo$ID[i]] conseguimos el equipo que jugo el encuentro con
    # el i-ésimo ID de df_equipo. Es decir, si pasa la condición es el j-ésimo equipo
    # y sino la pasa es porque es su rival en ese encuentro. Y por tanto, el j-ésimo equipo
    # es visitnate.
    
    
    datos$Punt_Loc[df_equipo$ID[i]] <- df_equipo$PT[i]
    datos$GF_Loc[df_equipo$ID[i]] <- df_equipo$GF[i]
    datos$GC_Loc[df_equipo$ID[i]] <- df_equipo$GF[i]
    # La variable DG, no es más que la difernecia entre GF - GC
    datos$DG_Loc[df_equipo$ID[i]] <- df_equipo$GF[i] - df_equipo$GC[i]
    datos$PG_Loc[df_equipo$ID[i]] <- df_equipo$PG[i]
    datos$PE_Loc[df_equipo$ID[i]] <- df_equipo$PE[i]
    datos$PP_Loc[df_equipo$ID[i]] <- df_equipo$PP[i]
  }else{
    # El j-ésimo equipo es visitnate
    datos$Punt_Vis[df_equipo$ID[i]] <- df_equipo$PT[i]
    datos$GF_Vis[df_equipo$ID[i]] <- df_equipo$GF[i]
    datos$GC_Vis[df_equipo$ID[i]] <- df_equipo$GC[i]
    datos$DG_Vis[df_equipo$ID[i]] <- df_equipo$GF[i] - df_equipo$GC[i]
    datos$PG_Vis[df_equipo$ID[i]] <- df_equipo$PG[i]
    datos$PE_Vis[df_equipo$ID[i]] <- df_equipo$PE[i]
    datos$PP_Vis[df_equipo$ID[i]] <- df_equipo$PP[i]
  }
  
  i <- i+1
  if(i == nrow(df_equipo)+1){ # Paramos cuando llegamos a la última jornada.
    break
  }
}

if(j == length(Equipos)){ # Paramos cuando llegamos al último equipo
  break
}

j <- j+1
}
# ________________________________________________________________________ #


############################################################################
#################### Puesto (ranking) ######################################
############################################################################

# Viendo el inicio de este apartado, el cual definimos este bucle de 1 a n_jornadas
# se prodría pensar, si el bucle tiene ese recorrido, ¿ Por qué no se ha efectuado
# con el bucle anterior que también erá por jornada.
# Bien, no es posible porque en el anterior ibamos  recorriendo para cada equipo
# una fila por jornada. En este bucle, trabajamos por cada s-ésima interacción
# con todas las filas de la jornada s-ésima.


# Ahora bien, en este apartado, lo que hacemos es asignar el puesto por jornada a 
# cada equipo, es decir, si en una temporada hay 18 equipos, hay que asignar a cada 
# equipo un valor entre 1 a 18, según la clasificación y los procedimiento de average.
# O sea, cada equipo tendrá un solo número, de esos 18, y ningún equipo tendrá
# un número que tenga otro equipo.


for(s in 1:n_jornadas){
  
  # Primero asignamos rangos a todas las jornadas, mediante los procedimientos de ida
  # definidos en el documento 'historia.html', y también se especifica en la
  # función ranking, que se encuentra en funciones_auxiliares.R
  
  order_rank <- ranking(datos,s) # aplicamos la función ranking
  
  # Guardamos el ranking del equipo local y del visitante de la s-ésima joranada
  datos$rank_Loc[datos$Jornada == s] <- order_rank$rank_Loc
  datos$rank_Vis[datos$Jornada == s] <- order_rank$rank_Vis
  
  # Obtenemos si la s-esima jornada estamos en la ida o la vuelta de la temporada
  camp <- datos$Campo [datos$Jornada == s] [1]

 if(camp>0){
   # Si estamos en la vuelta, tenemos que aplicar los procedimientos de average
   # que dicta el reglamento en esas jornadas
   
   df = datos %>% filter(Jornada == s) # Guardamos los datos de esa jornada
   temporada = datos %>% filter(Jornada <= s) # Guardamos los datos de la temporada
                                              # hasta esa jornada
   
   # Y aplicamos la función ranking2, estableciendolé, los parámetros df y temporada
   # citados, y además, aver_antig, el cual recordemos que es si el average que se aplica
   # es el antiguo (hasta 1995-96), o el nuevo (desde 1995-96 hasta la actualidad)
   daf =  ranking2(df =   df,temporada = temporada, l = aver_antig)
  
   # Guardamos en rank_LOc y rank_vis, los ranking con los procedimientos de average aplicados
   datos$rank_Loc[datos$Jornada == s] <- daf$rank_Loc
   datos$rank_Vis[datos$Jornada == s] <- daf$rank_Vis
 }
}

#_____________________________________________________________________________________#

#######################################################################################
###################### Preparación de la salida #######################################
#######################################################################################


# Guardamos dos tibbles, seleccionando las columnas dessedas.

clas_loc <- datos %>%
            select(ID, Jornada, Campo, Puesto = rank_Loc, Equipo = Local, 
                   Puntos = Punt_Loc, GF=  GF_Loc, GC= GC_Loc, DG= DG_Loc, PG = PG_Loc, 
                   PE = PE_Loc, PP = PP_Loc)
clas_Vis <- datos %>%
              select(ID, Jornada, Campo, Puesto = rank_Vis, Equipo = Visitante, 
                     Puntos = Punt_Vis, GF=  GF_Vis, GC= GC_Vis, DG= DG_Vis,  PG = PG_Vis, 
                     PE = PE_Vis, PP = PP_Vis)

# Este tibble lo usaremos para visualización.

Clasificacion = bind_rows(clas_loc, clas_Vis)%>%
                mutate(Temporada = subcarpeta[l]) %>%
                arrange(ID) %>%
                select(Temporada, Jornada:Puntos, PG, PE, PP, GF:DG) %>%
                arrange(Jornada, Puesto)%>% 
                as_tibble()


# Y este para seguir trabajando con las variables. Cuando le apliquemos las 
# siguientes transformaciones, será el tibble o lista que usemos para el modelo

Resultados = datos %>%
              mutate(Temporada = subcarpeta[l])%>%
              select(Temporada, Dia, Mes, Fecha, Jornada, Campo, Local, Visitante,
                     starts_with("Gol"), starts_with("Punt"),
                     starts_with("rank"), starts_with("GF"),
                     starts_with("GC"), starts_with("DG"), 
                     starts_with("PG")) %>%
              as_tibble()
######################################################################################              
# Finalmente guardamos, ambos tibble en una lista.

TEMPORADAS[[l]] <- list(Resultados = Resultados, Clasificacion = Clasificacion)
}

names(TEMPORADAS) <- subcarpeta
```








## Purrr {.div_rmd}

Ya que vamos a exportar los datos a archivos csv.  Podemos aprovechar que tenemos los datos en listas para usar funciones de `purrr`, y así ver un poco de **visualización**.

Vamos a mostrar el `histórico de campeones de liga`:

```{r}
# Empezampos seleccionando el conjuntos de listas de Clasificacion, también se puede hacer
# poniendo  2 en vez de Clasificacion, o usando pluck
map(TEMPORADAS, "Clasificacion") %>%
  # filtramos de cada una de las lista la jornada máxima y el puesto primero.
  map(~filter(.x, Jornada == max(Jornada) & Puesto==1 ))%>%
  # Seleccionamos la columna Equipo de cada una de las listas 
  map(~select(.x, Equipo))%>%
  # Si quisieramos ver al campeon de la temporada 2019-20, podriamos hacer map_dfr y
  # y luego count, pero no queremos ver ese dato, porque la temporado no a acabado.
  # Entonces sacamos el nombre de las listas, que contiene el nombre de las temporadas
  # con enframe
  enframe(~.x)%>%
  # Quitamos la temporada 2019-20
  filter(`~.x` != "2019-20") %>%
  # la variable value contiene 88 tibble de 1x1. hacemos unnest_longer para ver su contenido
  unnest_longer(value)%>%
  # y hacemos coount de la variable Equipo del tible y la ordenamos de manera decreciente,
  count(value$Equipo) %>%
  arrange(desc(n))
```

<div align= "center">
<h2> **Comparación**</h2>
<img src = "../img/palmares_campeon.png" >
<br>
<a href = "https://www.webprincipal.com/futbol/palmares/palmares.php" > Fuente </a>
<br>
</div>

Sin aplicar las `funciones_auxiliares` los resultados no serían iguales. Puesto que, ha habido varios campeonatos que se han decidido por el **average**.



## Exportacion {.div_rmd}

```{r}
# Exportamos nuestra lista a un archivo json
write_json(TEMPORADAS, "../data/TEMPORADAS.json")

# Transformamos cada conjunto de listas a data.frame y lo cuandamos en documentos csv.
resultados = map_df(TEMPORADAS,1)
clasificacion = map_df(TEMPORADAS,2)
write_csv(x = resultados, path = "../data/resultados.csv")
write_csv(clasificacion, path = "../data/clasificacion.csv")
```

<a href = "Memoria.html"> Volver a la guía </a>